import React from 'react';
import clsx from 'clsx';
import { IconButton, Typography } from '@material-ui/core';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import { makeStyles } from '@material-ui/core/styles';
import { BtnClearInput } from '../static/images';
import ArrowBackIosRoundedIcon from '@material-ui/icons/ArrowBackIosRounded';


const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        position: 'relative',
        justifyContent: 'center',
        alignItems: 'center',
        minHeight: 50,
        padding: '10px 55px',
        borderBottom: `1px solid ${theme.palette.grey[300]}`,
    },
    closeButton: {
        position: 'absolute',
        left: 3,
        top: '50%',
        transform: 'translateY(-50%)',
    },
    closeButtonIcon: {
        width: 20,
        height: 20,
        color: '#7f7f7f',
    }
}));

const Default = props => {
    const classes = useStyles();

    const { children } = props;

    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    return (
        <header className={clsx("header", classes.root)}>
            <Typography variant="h1">{children}</Typography>
            <IconButton aria-label="뒤로가기" className={classes.closeButton}>
                <ArrowBackIosRoundedIcon className={classes.closeButtonIcon} />
            </IconButton>
        </header>
    );
};

export default Default;