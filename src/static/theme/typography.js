import palette from './palette';

export default {
    fontFamily: ['"Nanum Gothic"', '"Malgun Gothic"', '"Sans-serif"'].join(','),
    fontSize: 15,
    h1: {
        fontWeight: 400,
        fontSize: 18,
        color: '#000',
    },
    h2: {
        fontWeight: 700,
        fontSize: 15,
        color: palette.dark.main,
    },
    body1: {
        fontSize: 15,
        '&.sizeSmall': {
            fontSize: 12,
        }
    },
}