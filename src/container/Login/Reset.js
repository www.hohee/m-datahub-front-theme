import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import { DialogContent, DialogActions, TextField, InputAdornment, FormControlLabel, RadioGroup, Radio, Typography, Button } from '@material-ui/core';
import { DialogTitle, ButtonClearInput, IconComplete } from '../../component';
/* import { Logo } from '../../static/images'; */

const useStyles = makeStyles((theme) => ({
    '@global': {
        '.formRow': {
            '& + .formRow, & + .boxRadioGroup': {
                marginTop: 10,
            }
        }
    },
    root: {
        marginTop: 40,
        '& .mainFormGroup': {
            marginTop: 60,
        }
    }
}));

const FindInfo = () => {
    const classes = useStyles();

    return (
        <React.Fragment>
            <DialogTitle>비밀번호 초기화</DialogTitle>
            <DialogContent id="dialogContent">
                <div className={classes.root}>
                    <div>
                        <Typography>등록된 이메일 주소 또는 휴대폰번호로 임시 비밀번호를 발송해 드립니다.</Typography>
                        <div className="mainFormGroup">
                            <div className="formRow">
                                <TextField
                                    id="userID"
                                    /* error
                                    helperText="아이디 오류" */
                                    placeholder="아이디 입력"
                                    InputProps={{
                                        endAdornment: (
                                            <InputAdornment position="end">
                                                <ButtonClearInput />
                                            </InputAdornment>
                                        ),
                                        classes: (
                                            {
                                                input: "input inputOutline",
                                                notchedOutline: "notchedOutline"
                                            }
                                        ),
                                        className: "inputBase"
                                    }}
                                    inputProps={
                                        { "title" : "아이디 입력" }
                                    }
                                    fullWidth
                                    variant="outlined"
                                    className={clsx('textField iconTextField typeIcoUserId sizeLg')}
                                />
                            </div>

                            <RadioGroup className="boxRadioGroup">
                                <FormControlLabel value="rdo1" className="boxRadioControl" classes={{label: 'boxRadioLabel'}} control={<Radio icon={""} checkedIcon={""} className="boxRadio" />} label="휴대폰으로 받기" />
                                <FormControlLabel value="rdo2" className="boxRadioControl" classes={{label: 'boxRadioLabel'}} control={<Radio icon={""} checkedIcon={""} className="boxRadio" />} label="이메일주소로 받기" />
                            </RadioGroup>
                        </div>
                        <div className="pageActions">
                            <Button variant="contained" size="large" fullWidth className="btnConfirm">확인</Button>
                        </div>
                    </div>

                    <div>
                        <div className="completeMsgArea">
                            <IconComplete className="icoComplete" />
                            <Typography>등록된 휴대폰<br /> <Typography color="primary" component="strong" className="txtBold">010-3456-****</Typography> 으로<br /> 임시 비밀번호를 발송하였습니다.</Typography>
                        </div>
                        <div className="pageActions">
                            <Button variant="contained" size="large" fullWidth className="btnConfirm">로그인</Button>
                        </div>
                    </div>
                </div>
            </DialogContent>
        </React.Fragment>
    );
};

export default FindInfo;