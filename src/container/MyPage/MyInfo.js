import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import { Link, Grid, Card, CardContent, IconButton, Typography, Button, Chip } from '@material-ui/core';
import { Header } from '../../component';
import { IcoMyPage, IcoEdit } from '../../static/images';

const useStyles = makeStyles((theme) => ({
    userInfo: {
        paddingTop: 40,
        paddingBottom: 40,
    },
    userName: {
        marginRight: 15,
        fontWeight: 300,
        fontSize: 24,
        lineHeight: 1,
        verticalAlign: 'middle',
        color: '#000',
    },
    btnEdit: {
        position: 'absolute',
        right: 20,
        top: 15,
        width: 34,
        height: 34,
        background: `rgba(107, 123, 253, 0.1) url(${IcoEdit}) no-repeat center`,
        backgroundSize: 15,
    },
}));

const MyInfo = () => {
    const classes = useStyles();

    return (
        <React.Fragment>
            <Header isBack>내정보 관리</Header>
            <div className="contents">
                <div className={clsx(classes.userInfo, "infoPanel")}>
                    <Typography className="infoHead"><span className={classes.userName}>공지철</span><Chip label="정상" className="chipLabel2" /></Typography>
                    <div className="infoGroup overview">
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">사번</Grid>
                            <Grid item className="infoDesc">00000000000000</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">아이디</Grid>
                            <Grid item className="infoDesc">ggonggong</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">사용등록일</Grid>
                            <Grid item className="infoDesc">2019.03.01</Grid>
                        </Grid>
                    </div>
                </div>
                <div className="infoPanel">
                    <Typography variant="h2" className="infoHead">기본정보</Typography>
                    <div className="infoGroup">
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">이름</Grid>
                            <Grid item className="infoDesc">공지철</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">휴대폰번호</Grid>
                            <Grid item className="infoDesc">010-****-5678</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">이메일주소</Grid>
                            <Grid item className="infoDesc">gonggong@globalfm.com</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">생년월일</Grid>
                            <Grid item className="infoDesc">701010-1******</Grid>
                        </Grid>
                    </div>
                    <IconButton className={classes.btnEdit}><Typography variant="srOnly">기본정보수정</Typography></IconButton>
                </div>
                <div className="infoPanel">
                    <Typography variant="h2" className="infoHead">소속정보</Typography>
                    <div className="infoGroup">
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">GA명</Grid>
                            <Grid item className="infoDesc">글로벌 금융판매</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">소속지점명</Grid>
                            <Grid item className="infoDesc">S&amp;P &gt; 강남지사 &gt; 신사동지점</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">협회코드</Grid>
                            <Grid item className="infoDesc">YYYY0000000000000</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">영업구분</Grid>
                            <Grid item className="infoDesc">순수내근</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">직책</Grid>
                            <Grid item className="infoDesc">설계사</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">직급</Grid>
                            <Grid item className="infoDesc">과장</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">자격</Grid>
                            <Grid item className="infoDesc">명인</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">영업속성</Grid>
                            <Grid item className="infoDesc">라운지</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">소득구분</Grid>
                            <Grid item className="infoDesc">근로</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">유치자코드</Grid>
                            <Grid item className="infoDesc"></Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">위촉/해촉</Grid>
                            <Grid item className="infoDesc">위촉</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">재직구분</Grid>
                            <Grid item className="infoDesc">재직</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">위촉일</Grid>
                            <Grid item className="infoDesc">2011-11-11</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">해촉일</Grid>
                            <Grid item className="infoDesc"></Grid>
                        </Grid>
                    </div>
                </div>
                <div className="infoPanel">
                    <Typography variant="h2" className="infoHead">설계사 추가정보</Typography>
                    <div className="infoGroup">
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">우수인증설계사</Grid>
                            <Grid item className="infoDesc">Y</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">모집경력시스템동의</Grid>
                            <Grid item className="infoDesc">Y</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">변액자격여부</Grid>
                            <Grid item className="infoDesc">Y</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">변액자격취득일</Grid>
                            <Grid item className="infoDesc">2010-08-10</Grid>
                        </Grid>
                    </div>
                </div>
                <div className="infoPanel">
                    <Typography variant="h2" className="infoHead">마케팅 동의</Typography>
                    <div className="infoGroup">
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">마케팅동의버전</Grid>
                            <Grid item className="infoDesc">V 1.0</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">마케팅동의 종료일</Grid>
                            <Grid item className="infoDesc">2020-10-10</Grid>
                        </Grid>
                    </div>
                </div>
                <div className="pageActions">
                    <Button fullWidth color="primary" variant="contained">보험사코드 보기</Button>
                </div>
            </div>
            {/* <div className="stickyBottom">
                <Button variant="contained" fullWidth className="btnConfirm">버튼</Button>
            </div> */}
        </React.Fragment>
    );
};

export default MyInfo;