import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import { Dialog, DialogTitle, DialogContent, DialogActions, Typography, IconButton, Button, Slide } from '@material-ui/core';

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const Default = props => {
    const { children } = props;

    return (
        <React.Fragment>
            <Dialog fullScreen aria-labelledby="dialogTitle" aria-describedby="dialogContent" TransitionComponent={Transition} open>
                {children}
            </Dialog>
        </React.Fragment>
    );
};

Default.propTypes = {
    children: PropTypes.node
};

export default Default;