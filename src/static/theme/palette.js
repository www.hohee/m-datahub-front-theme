export default {
    primary: {
        main: '#1fb6ff',
        dark: '#14a1ff',
    },
    secondary: {
        main: '#6b7bfd'
    },
    tertiary: {
        main: '#757f95'
    },
    dark: {
        main: '#343d4d'
    },
    error: {
        main: '#d4320e',
    },
    /* warning: {}, */
    grey: {
        100: '#f5f5f5',
        200: '#f8f9fc',
        300: '#eee',
        400: '#ddd',
        500: '#ccc',
        600: '#999',
    },
    divider: '#eaedf6',
    text: {
        primary: '#333',
        secondary: '#666',
        /*
        disabled: ,
        hint: ,
        */
    },
    background: {
        default: '#fff',
        //default: 'pink',
    },
    backgroundGradient: 'linear-gradient(45deg, rgba(114,145,243,1) 0%, rgba(114,145,243,1) 55%, rgba(159,135,244,1) 100%)',
}