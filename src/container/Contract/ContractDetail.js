import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import { Link, Grid, Card, CardContent, IconButton, Typography, Button, Chip } from '@material-ui/core';
import { Header } from '../../component';
import { IcoMyPage, IcoEdit } from '../../static/images';

const useStyles = makeStyles((theme) => ({

}));

const ContractDetail = () => {
    const classes = useStyles();

    return (
        <React.Fragment>
            <Header isBack>보유계약상세</Header>
            <div className="contents">
                <div className="infoPanel">
                    <Grid container justify="flex-end" className="row" style={{ marginBottom: 15, }}>
                        <Chip label="승인요청" className="chipLabel1" />
                        <Chip label="승인완료" className="chipLabel2" />
                        <Chip label="반려" className="chipLabel3" />
                        <Chip label="거절" className="chipLabel4" />
                        <Chip label="실효" className="chipLabel5" />
                    </Grid>
                    <div className="infoGroup overview">
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">증권번호</Grid>
                            <Grid item className="infoDesc">11452898112450</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">최종처리</Grid>
                            <Grid item className="infoDesc">홍길동 / 2019-11-11 13:20:45</Grid>
                        </Grid>
                    </div>
                </div>
                <div className="infoPanel">
                    <Typography variant="h2" className="infoHead">청약보험정보</Typography>
                    <div className="infoGroup">
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">상품구분</Grid>
                            <Grid item className="infoDesc">손보보장</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">보험사</Grid>
                            <Grid item className="infoDesc">KB손해보험</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">상품명</Grid>
                            <Grid item className="infoDesc">참좋은건강보험</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">계약일시</Grid>
                            <Grid item className="infoDesc">2019-10-20</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">보험기간</Grid>
                            <Grid item className="infoDesc">15년만기</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">납입기간</Grid>
                            <Grid item className="infoDesc">15년납</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">총보험료</Grid>
                            <Grid item className="infoDesc">22,300원</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">납입주기</Grid>
                            <Grid item className="infoDesc">월납</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">월납보험료</Grid>
                            <Grid item className="infoDesc">22,300원</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">환산보험료 1차년</Grid>
                            <Grid item className="infoDesc">22,300원</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">환산보험료 2차년</Grid>
                            <Grid item className="infoDesc">22,300원</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">환산보험료 3차년</Grid>
                            <Grid item className="infoDesc">22,300원</Grid>
                        </Grid>
                    </div>
                </div>
                <div className="infoPanel">
                    <Typography variant="h2" className="infoHead">보험대상자</Typography>
                    <div className="infoGroup">
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">계약자</Grid>
                            <Grid item className="infoDesc">홍길남</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">계약자 주민번호</Grid>
                            <Grid item className="infoDesc">1980301-1******</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">피보험자</Grid>
                            <Grid item className="infoDesc">홍길부</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">피보험자 주민번호</Grid>
                            <Grid item className="infoDesc">1960301-1******</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">피보험자 연령</Grid>
                            <Grid item className="infoDesc">보험연령 70세</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">피보험자 상령일</Grid>
                            <Grid item className="infoDesc">2020-10-01 (D-291)</Grid>
                        </Grid>
                    </div>
                </div>
                <div className="infoPanel">
                    <Typography variant="h2" className="infoHead">담당설계사</Typography>
                    <div className="infoGroup">
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">모집자(사번)</Grid>
                            <Grid item className="infoDesc">홍길순(00000000000000)</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">모집사 소속</Grid>
                            <Grid item className="infoDesc">S&amp;P &gt; 강남지사 &gt; 신사동지점</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">수금자(사업)</Grid>
                            <Grid item className="infoDesc">홍길순(00000000000000)</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">수금자 소속</Grid>
                            <Grid item className="infoDesc">S&amp;P &gt; 강남지사 &gt; 신사동지점</Grid>
                        </Grid>
                    </div>
                </div>
            </div>
            <div className="stickyBottom">
                <Button variant="contained" fullWidth className="btnConfirm">목록보기</Button>
            </div>
        </React.Fragment>
    );
};

export default ContractDetail;