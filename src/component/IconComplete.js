import React from 'react';
import clsx from 'clsx';
/* import { IconButton, Typography } from '@material-ui/core'; */
import { makeStyles } from '@material-ui/core/styles';
import { IcoCompleteCircle, IcoCompleteCheck } from '../static/images';

const useStyles = makeStyles((theme) => ({
    '@global': {
        
    },

    complete: {
        position: 'relative',
        overflow: 'hidden',
        width: 57,
        height: 57,
        margin: '0 auto',
    },

    circle: {
        width: 55,
        height: 55,
        background: `url(${IcoCompleteCircle}) no-repeat center`,
        backgroundSize: '55px',
        transform: 'rotate(45deg)',
        '&:before': {
            display: 'block',
            position: 'absolute',
            left: '50%',
            top: 0,
            width: '50%',
            height: '100%',
            background: `url(${IcoCompleteCircle}) no-repeat right center`,
            backgroundSize: 55,
            zIndex: 2,
            content: '""',
        }
    },
    mask: {
        '&:before, &:after': {
            display: 'block',
            position: 'absolute',
            top: 0,
            width: '50%',
            height: '100%',
            padding: 2,
            backgroundColor: theme.palette.grey[200],
            boxSizing: 'content-box',
            content: '""',
        },
        '&:before': {
            left: -1,
            transformOrigin: '100% 50%',
            animation: '$mask 200ms 300ms linear forwards',
        },
        '&:after': {
            left: 'calc(50% - 1px)',
            transformOrigin: '0 50%',
            zIndex: 2,
            animation: '$mask 200ms 100ms linear forwards',
        }
    },
    icoCheck: {
        position: 'absolute',
        top: 11,
        left: 17,
        width: 0,
        height: 24,
        background: `url(${IcoCompleteCheck}) no-repeat 0 0`,
        backgroundSize: '34px 24px',
        zIndex: 5,
        animation: '$check 250ms 500ms cubic-bezier(0.65, 0.05, 0.36, 1) forwards',
    },

    '@keyframes mask': {
        '0%': {
            transform: 'rotate(0)',
        },
        '99%': {
            transform: 'rotate(180deg)',
            opacity: 1,
        },
        '100%': {
            opacity: 0,
        }
    },
    '@keyframes check': {
        '0%': {
            width: 0,
        },
        '100%': {
            width: 34,
        },
    },
}));

const IconComplete = props => {
    const classes = useStyles();

    const { className } = props;

    return (
        <React.Fragment>
            <div className={clsx(classes.complete, className)}>
                <div className={classes.circle}>
                    <div className={classes.mask}></div>
                </div>
                <div className={classes.icoCheck}></div>
            </div>
        </React.Fragment>
    );
};

export default IconComplete;