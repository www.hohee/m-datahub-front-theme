import React from 'react';
import clsx from 'clsx';
import { Grid, Slide, Dialog, DialogContent, DialogActions, Button, Typography, IconButton, RadioGroup, FormControlLabel, Radio, TextField } from '@material-ui/core';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { BtnClosePopup } from '../static/images';

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const style = theme => ({
    root: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: 46,
        padding: '10px 55px',
        borderBottom: `1px solid ${theme.palette.grey[300]}`,
        fontSize: 17,
        color: '#000',
    },
    btnClose: {
        position: 'absolute',
        left: 0,
        top: 0,
        width: 45,
        height: 45,
        background: `url(${BtnClosePopup}) no-repeat center`,
        backgroundSize: 15,
    },
});

const DialogTitle = withStyles(style)(props => {
    const { children, classes, ...other } = props;
    
    return (
        <MuiDialogTitle disableTypography className={classes.root} {...other}>
            <Typography variant="h1">{children}</Typography>
            <IconButton aria-label="닫기" className={classes.btnClose}></IconButton>
        </MuiDialogTitle>
    );
});

const useStyles = makeStyles(theme => ({
    container: {
        alignItems: 'flex-end',
    },
    paper: {
        height: '48.8%',
        borderTopLeftRadius: '8px 6px',
        borderTopRightRadius: '8px 6px',
    },
    contents: {
        paddingTop: 15,
        paddingBottom: 45,
    },
    btnText: {
        position: 'absolute',
        right: 0,
        top: 9,
        minWidth: 0,
        padding: '0 15px',
        fontSize: 14,
        color: '#666',
    },
    sortGroup: {
        marginTop: 25,
    },
    pickDateArea: {
        height: 37,
        overflow: 'hidden',
        marginTop: 10,
        border: `1px solid ${theme.palette.grey[400]}`,
        borderRadius: 2,
        '& .inputBase': {
            fontSize: 15,
        },
    },
    pickDate: {
        width: '45%',
        textAlign: 'center',
        '& input': {
            textAlign: 'center',
        }
    },
    blank: {
        width: '10%',
        textAlign: 'center',
    },
}));

const SortingPopup = props => {
    const classes = useStyles();

    const [ open, setOpen ] = React.useState(true);

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition} classes={{ container: classes.container, paper: classes.paper }}>
            <DialogTitle>조회선택</DialogTitle>
            <DialogContent className={classes.contents}>
                <div className={classes.sortGroup}>
                    <Typography variant="h2" className="formLabel">조회기간</Typography>
                    <RadioGroup className="boxRadioGroup">
                        <FormControlLabel value="rdo1" className="boxRadioControl sizeSmall" classes={{label: 'boxRadioLabel'}} control={<Radio icon={""} checkedIcon={""} className="boxRadio" />} label="1개월" />
                        <FormControlLabel value="rdo2" className="boxRadioControl sizeSmall" classes={{label: 'boxRadioLabel'}} control={<Radio icon={""} checkedIcon={""} className="boxRadio" />} label="2개월" />
                        <FormControlLabel value="rdo3" className="boxRadioControl sizeSmall" classes={{label: 'boxRadioLabel'}} control={<Radio icon={""} checkedIcon={""} className="boxRadio" />} label="3개월" />
                        <FormControlLabel value="rdo4" className="boxRadioControl sizeSmall" classes={{label: 'boxRadioLabel'}} control={<Radio icon={""} checkedIcon={""} className="boxRadio" />} label="직접입력" />
                    </RadioGroup>
                    <Grid container alignItems="center" className={classes.pickDateArea}>
                        <Grid item className={classes.pickDate}>
                            <TextField
                                id="startD"
                                type="date"
                                defaultValue="2019-10-11"
                                InputProps={{
                                    className: "inputBase",
                                    disableUnderline: true
                                }}
                                inputProps={
                                    { "title" : "조회시작일 입력" }
                                }
                                //fullWidth
                                className={clsx('textField')}
                            />
                        </Grid>
                        <Grid item className={classes.blank}>~</Grid>
                        <Grid item className={classes.pickDate}>
                            <TextField
                                id="startD"
                                type="date"
                                defaultValue="2019-10-11"
                                InputProps={{
                                    className: "inputBase",
                                    disableUnderline: true
                                }}
                                inputProps={
                                    { "title" : "조회시작일 입력" }
                                }
                                //fullWidth
                                className={clsx('textField')}
                            />
                        </Grid>
                    </Grid>
                </div>
                <div className={classes.sortGroup}>
                    <Typography variant="h2" className="formLabel">처리상태</Typography>
                    <RadioGroup className="boxRadioGroup">
                        <FormControlLabel value="rdo5" className="boxRadioControl sizeSmall" classes={{label: 'boxRadioLabel'}} control={<Radio icon={""} checkedIcon={""} className="boxRadio" />} label="전체" />
                        <FormControlLabel value="rdo6" className="boxRadioControl sizeSmall" classes={{label: 'boxRadioLabel'}} control={<Radio icon={""} checkedIcon={""} className="boxRadio" />} label="승인요청" />
                        <FormControlLabel value="rdo7" className="boxRadioControl sizeSmall" classes={{label: 'boxRadioLabel'}} control={<Radio icon={""} checkedIcon={""} className="boxRadio" />} label="승인완료" />
                        <FormControlLabel value="rdo8" className="boxRadioControl sizeSmall" classes={{label: 'boxRadioLabel'}} control={<Radio icon={""} checkedIcon={""} className="boxRadio" />} label="반려/거절" />
                    </RadioGroup>
                </div>
                <Button onClick={handleClose} className={classes.btnText}>확인</Button>
            </DialogContent>
        </Dialog>
    );
};

export default SortingPopup;