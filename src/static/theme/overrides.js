import palette from './palette';
import { IcoInput, IcoGuideMsg, IcoMyPage, IcoArrowRight } from '../../static/images';

export default {
    MuiCssBaseline: {
        '@global': {
            'html, body, #root': {
                height: '100%',
            },
            'dl, dt, dd': {
                margin: 0,
            },
            'img': {
                verticalAlign: 'top',
            },
            '.pageActions': {
                marginTop: 30,
            },
            '.completeMsgArea': {
                padding: '40px 0',
                backgroundColor: palette.grey[200],
                textAlign: 'center',
                '& .icoComplete': {
                    marginBottom: 20,
                }
            },
            '.infoPanel': {
                position: 'relative',
                margin: '0 -20px 10px',
                padding: '29px 20px 23px',
                borderBottom: `1px solid ${palette.divider}`,
                backgroundColor: '#fff',
                '& .infoHead': {
                    marginBottom: 16,
                },
            },
            '.infoGroup': {
                width: '100%',
                '& .row + .row': {
                    marginTop: 12,
                },
                '&.overview': {
                    '& .row': {
                        position: 'relative',
                        paddingLeft: 11,
                        '&:before': {
                            display: 'block',
                            position: 'absolute',
                            left: 0,
                            top: 9,
                            width: 3,
                            height: 3,
                            borderRadius: '50%',
                            backgroundColor: 'rgba(0, 0, 0, 0.2)',
                            content: '""',
                        }
                    },
                    '& .infoSubhead': {
                        color: palette.text.secondary,
                    },
                    '& .infoDesc': {
                        color: '#000',
                    },
                },
            },
            '.infoSubhead': {
                color: palette.tertiary.main,
            },
            '.formGroup': {
                '& .textField': {
                    marginTop: -1,
                    '& .notchedOutline': {
                        borderRadius: 0,
                    },
                    '&:first-child .notchedOutline': {
                        borderTopLeftRadius: 2,
                        borderTopRightRadius: 2,
                    },
                    '&:last-child .notchedOutline': {
                        borderBottomLeftRadius: 2,
                        borderBottomRightRadius: 2,
                    },
                },
            },
            '.fakeInput': {
                justifyContent: 'flex-start',
                height: 50,
                paddingLeft: 43,
                border: `1px solid ${palette.grey[400]}`,
                borderRadius: 2,
                backgroundColor: '#fff',
                textAlign: 'left',
                '&:before': {
                    display: 'block',
                    position: 'absolute',
                    left: 14,
                    top: '50%',
                    width: 17,
                    height: 24,
                    marginTop: -12,
                    background: `url(${IcoInput}) no-repeat -54px 0`,
                    backgroundSize: '128px auto',
                    content: '""',
                },
                '&[type="button"]': {
                    paddingRight: 36,
                    '&:after': {
                        display: 'block',
                        position: 'absolute',
                        right: 15,
                        top: '50%',
                        width: 7,
                        height: 12,
                        marginTop: -6,
                        background: `url(${IcoArrowRight}) no-repeat 0 0`,
                        backgroundSize: '7px auto',
                        content: '""',
                    },
                },
            },
            '.contractCard': {
                position: 'relative',
                width: '100%',
                padding: 20,
                paddingLeft: 20,
                backgroundColor: '#fff',
                '&:hover': {
                    backgroundColor: '#fff',
                },
                '& + .contractCard': {
                    marginTop: 15,
                },
                '& .contractCardHead': {
                    marginBottom: 10,
                    fontWeight: 400,
                    color: palette.dark.main,
                },
                '& .contractCardDesc': {
                    textAlign: 'right',
                    '& .value': {
                        fontWeight: '700',
                        fontSize: 18,
                        color: '#000',
                    },
                    '& + .contractCardDesc': {
                        marginTop: 8,
                    }
                },
                '& .contractCardSubdesc': {
                    fontSize: 11,
                    color: palette.text.secondary,
                },
                '& .contractCardDetail': {
                    margin: '20px -20px 0 -20px',
                    padding: '24px 20px 0',
                    borderTop: `1px solid ${palette.grey[200]}`,
                    '& .row': {
                        display: 'flex',
                        alignItems: 'center',
                        marginTop: 8,
                        lineHeight: 1,
                        '&:first-child': {
                            marginTop: 0,
                        },
                        '& [class*="chipLabel"]': {
                            marginRight: 11,
                        },
                        '& .blank': {
                            marginLeft: 10,
                            marginRight: 10,
                            color: '#ddd',
                        }
                    },
                },

                '&.hasIcon': {
                    paddingLeft: 45,
                    '&:before': {
                        display: 'block',
                        position: 'absolute',
                        left: 20,
                        top: 20,
                        width: 16,
                        height: 17,
                        background: `url(${IcoMyPage}) no-repeat -47px 0`,
                        backgroundSize: '150px',
                        content: '""',
                    },
                    '&.icoNewContract:before': {
                        backgroundPosition: '-21px 0',
                    },
                    
                    '& .contractCardDetail': {
                        marginLeft: -45,
                    },
                },
            }
        },
    },

    MuiFormGroup: {
        root: {
            '&.boxRadioGroup': {
                flexDirection: 'row',
            }
        },
    },
    MuiFormControlLabel: {
        root: {
            marginLeft: -9,
            marginRight: 0,
            '&.boxRadioControl': {
                position: 'relative',
                height: 45,
                margin: 0,
                marginLeft: -1,
                flex: 1,
                '&:first-child': {
                    marginLeft: 0,
                    '& .radio': {
                        borderTopLeftRadius: 2,
                        borderBottomLeftRadius: 2,
                    },
                },
                '&:last-child': {
                    '& .radio': {
                        borderTopRightRadius: 2,
                        borderBottomRightRadius: 2,
                    },
                },
                '& .boxRadioLabel': {
                    position: 'relative',
                    width: '100%',
                    fontSize: 15,
                    textAlign: 'center',
                    color: palette.text.secondary,
                    zIndex: 1,
                },

                '&.sizeSmall': {
                    height: 37,
                    '& .boxRadio': {
                        backgroundColor: '#fff',
                    }
                }
            }
        },
    },
    MuiRadio: {
        root: {
            '&.boxRadio': {
                position: 'absolute',
                left: 0,
                top: 0,
                width: '100%',
                height: '100%',
                padding: 0,
                border: `1px solid ${palette.grey[400]}`,
                borderRadius: 0,
                background: palette.grey[200],
            },

            '&$checked': {
                '&.boxRadio': {
                    borderColor: palette.primary.main,
                    backgroundColor: palette.grey[200],
                    zIndex: 1,
                    transition: 'border 150ms linear',
                    '&:hover': {
                        backgroundColor: palette.grey[200],
                    }
                },
                '& + .boxRadioLabel': {
                    color: palette.primary.main,
                }
            }
        },
    },

    MuiTextField: {
        root: {
            '& .input': {
                fontSize: 15,
                lineHeight: 1.2,
            },

            '&.sizeLg': {
                '& input': {
                    padding: 16,
                }
            },

            '&.iconTextField': {
                '& .inputBase': {
                    height: 50,
                },
                '& .inputBase:before': {
                    display: 'block',
                    position: 'absolute',
                    left: 14,
                    top: '50%',
                    width: 17,
                    height: 24,
                    marginTop: -12,
                    backgroundImage: `url(${IcoInput})`,
                    backgroundSize: '128px auto',
                    content: '""',
                },
                '& .input': {
                    paddingLeft: 43,
                    paddingRight: 0,
                },

                '&.typeIcoUserPw .inputBase:before': {
                    backgroundPositionX: -27
                },

                '&.typeIcoPhone .inputBase:before': {
                    backgroundPositionX: -54
                },

                '&.typeIcoEmail .inputBase:before': {
                    left: 13,
                    width: 20,
                    backgroundPositionX: -79
                },

                '&.typeIcoCheck .inputBase:before': {
                    backgroundPositionX: -111
                },
            },
        }
    },

    MuiOutlinedInput: {
        root: {
            backgroundColor: '#fff',
            '&$focused': {
                zIndex: 1,
                '& .notchedOutline': {
                    borderWidth: 1,
                    borderColor: palette.dark.main,
                }
            },
            '&:hover': {
                '& .notchedOutline': {
                    borderColor: palette.grey[400],
                }
            },
        },
        notchedOutline: {
            top: 0,
            borderColor: palette.grey[400],
            borderRadius: 2,
            '& legend': {
                display: 'none',
            }
        },
        adornedEnd: {
            paddingRight: 0,
            '& .timeLimit': {
                marginRight: 12,
            },
            '& .btnInForm': {
                marginRight: 10,
            },
        }
    },

    MuiFormHelperText: {
        contained: {
            marginTop: 10,
            marginLeft: 0,
            fontSize: 12,
            lineHeight: '14px',
            '&$error': {
                position: 'relative',
                paddingLeft: 21,
                '&:before': {
                    display: 'block',
                    position: 'absolute',
                    left: 0,
                    top: 0,
                    width: 14,
                    height: 14,
                    background: `url(${IcoGuideMsg}) no-repeat 0 0`,
                    backgroundSize: '50px',
                    content: '""',
                }
            },
        },
    },

    MuiCheckbox: {
        root: {
            '& .icon': {
                width: 18,
                height: 18,
                border: `1px solid ${palette.grey[500]}`,
                borderRadius: 2,
                '&.iconChecked': {
                    position: 'relative',
                    borderColor: palette.primary.main,
                    '&:before, &:after': {
                        display: 'block',
                        position: 'absolute',
                        height: 2,
                        borderRadius: 2,
                        backgroundColor: palette.primary.main,
                        content: '""',
                    },
                    '&:before': {
                        width: 6,
                        top: 8,
                        left: 2,
                        transform: 'rotate(45deg)',
                    },
                    '&:after': {
                        width: 10,
                        top: 6,
                        left: 5,
                        transform: 'rotate(-45deg)',
                    },
                },
            },

            '&.sizeLarge': {
                '& .icon': {
                    width: 26,
                    height: 26,
                    '&.iconChecked': {
                        '&:before': {
                            width: 6,
                            top: 13,
                            left: 6,
                        },
                        '&:after': {
                            width: 10,
                            top: 11,
                            left: 9,
                        },
                    }
                },
            },

            '&.rounded': {
                '& .icon': {
                    borderRadius: '50%',
                }
            },

            '&.invert': {
                '& .icon.iconChecked': {
                    backgroundColor: palette.primary.main,
                    '&:before, &:after': {
                        backgroundColor: '#fff',
                    }
                }
            }
        },
    },

    MuiButtonGroup: {
        root: {
            '&.disableElevation': {
                boxShadow: 'none',
                '&:hover': {
                    boxShadow: 'none !important',
                    '@media (hover: none)': {
                        boxShadow: 'none',
                    }
                },
                '&$focusVisible': {
                    boxShadow: 'none',
                },
                '&:active': {
                    boxShadow: 'none',
                },
                '&$disabled': {
                    boxShadow: 'none',
                },
            },
            '&.btnGroup': {
                '& button:first-child': {
                    borderTopLeftRadius: 2,
                    borderBottomLeftRadius: 2,
                },
                '& button:last-child': {
                    borderTopRightRadius: 2,
                    borderBottomRightRadius: 2,
                },
                '& button:not(:first-child)': {
                    borderLeftColor: 'transparent',
                    '&:before': {
                        display: 'block',
                        position: 'absolute',
                        left: -1,
                        top: 8,
                        bottom: 8,
                        width: 1,
                        backgroundColor: palette.grey[400],
                        content: '""',
                    }
                },
            },
        },
    },

    MuiButton: {
        root: {
            /* '&.contractCard': {
                '& + .contractCard': {
                    marginTop: 15,
                },
                width: '100%',
                padding: '25px 20px',
                backgroundColor: '#fff',
                textAlign: 'left',
                '&:hover': {
                    backgroundColor: '#fff',
                },
                '& .infoGroup': {
                    width: '100%',
                }
            }, */
            boxShadow: 'none',
            '&:hover': {
                boxShadow: 'none',
                '@media (hover: none)': {
                    boxShadow: 'none',
                }
            },
            '&$focusVisible': {
                boxShadow: 'none',
            },
            '&:active': {
                boxShadow: 'none',
            },
            '&$disabled': {
                backgroundColor: palette.grey[400],
                color: '#fff',
                boxShadow: 'none',
            }
        },
        sizeLarge: {
            height: 50,
        },

        text: {
            '&.btnLinkText': {
                fontSize: 12,
                '& .endIcon': {
                    marginLeft: 1,
                },
                '& .icoLinkTextArr': {
                    width: 18,
                    height: 17,
                    color: palette.grey[500],
                }
            },
        },

        outlined: {
            border: `1px solid ${palette.grey[400]}`,
            borderRadius: 2,
            backgroundColor: palette.grey[200],
            color: palette.text.primary,
            verticalAlign: 'top',
            boxShadow: 'none',
            '&:hover': {
                backgroundColor: palette.grey[200],
            }
        },
        outlinedSizeLarge: {
            fontSize: 15,
        },

        contained: {
            height: 50,
            borderRadius: 2,
            fontWeight: '700',
            boxShadow: 'none',
            '&.btnConfirm': {
                backgroundColor: palette.dark.main,
                color: '#fff',
                '&:hover': {
                    backgroundColor: palette.dark.main,
                },
            },
            '&.btnInForm': {
                minWidth: 0,
                height: 30,
                paddingLeft: 10,
                paddingRight: 10,
                fontWeight: 400,
                fontSize: 12,
            },
            '&:hover': {
                boxShadow: 'none',
                '@media (hover: none)': {
                    boxShadow: 'none',
                }
            },
            '&$focusVisible': {
                boxShadow: 'none',
            },
            '&:active': {
                boxShadow: 'none',
            },
            '&$disabled': {
                backgroundColor: palette.grey[400],
                color: '#fff',
                boxShadow: 'none',
                '&.btnConfirm': {
                    backgroundColor: palette.grey[400],
                    color: '#fff',
                },
            },
        },
        containedPrimary: {
            color: '#fff',
        },
        containedSizeLarge: {
            borderRadius: 2,
            fontWeight: '700',
            fontSize: 15,  
        },
    },

    MuiTypography: {
        root: {
            '&.txtBold': {
                fontWeight: '700',
            },
            '&.sizeXLarge': {
                fontSize: 24,
                lineHeight: 1.4,
            },
            '&.formLabel': {
                marginBottom: 10,
                fontWeight: 400,
                fontSize: 13,
            },
            '&.msgGuide': {
                position: 'relative',
                paddingLeft: 21,
                '&:before': {
                    display: 'block',
                    position: 'absolute',
                    left: 0,
                    top: 0,
                    width: 14,
                    height: 14,
                    background: `url(${IcoGuideMsg}) no-repeat 0 0`,
                    backgroundSize: '50px',
                    content: '""',
                },
                '&.typeError:before': {
                    backgroundPosition: '0 0',
                },
                '&.typeInfo:before': {
                    backgroundPosition: '-19px 0',
                },
                '&.sizeSmall': {
                    lineHeight: '14px',
                }
            }
        }
    },

    MuiDialog: {
        paper: {
            '&.alertPaper': {
                width: 295,
                margin: 0,
                borderRadius: 2,
            },
        },
    },

    MuiDialogContent: {
        root: {
            padding: '0 20px',
            '&.alertContent': {
                padding: '36px 0 35px',
                fontSize: 16,
                textAlign: 'center',
                color: '#000',
            },
        },
    },

    MuiDialogContentText: {
        root: {
            '&.alertContentText': {
                marginBottom: 0,
                fontSize: 16,
                textAlign: 'center',
                color: '#000',
            },
        }
    },

    MuiDialogActions: {
        root: {
            padding: 0,
            '& .btnConfirm': {
                width: '100%',
                height: 55,
                borderLeft: '1px solid rgba(248, 249, 252, 0.1)',
                borderRadius: 0,
                fontWeight: '700',
                '&:first-child': {
                    borderLeft: 'none',
                }
            },
            '&.alertActions': {
                '& .btnConfirm': {
                    height: 45,
                    fontWeight: '400',
                },
            },
        },
    },

    MuiTabs: {
        root: {
            minHeight: 41,
        },
        flexContainer: {
            borderBottom: `1px solid ${palette.divider}`,
        },
        indicator: {
            display: 'flex',
            justifyContent: 'center',
            backgroundColor: 'transparent',
            '& .bar': {
                borderRadius: 2,
                backgroundColor: palette.primary.main,
                transition: 'width 100ms linear',
            }
        },
    },

    MuiTab: {
        root: {
            minHeight: 41,
            fontWeight: '400',
            fontSize: 15,
            '&:before': {
                display: 'block',
                position: 'absolute',
                left: 0,
                top: 14,
                bottom: 14,
                width: 1,
                backgroundColor: palette.grey[300],
                content: '""',
            },
            '&:first-child:before': {
                display: 'none',
            }
        },
        textColorInherit: {
            opacity: 1,
            '&$selected': {
                color: '#000',
            }
        },
        wrapper: {
            width: 'auto',
        }
    },
    MuiCard: {
        root: {
            
        }
    },
    MuiCardHeader: {
        root: {
            padding: 0,
        }
    },
    MuiCardContent: {
        root: {
            padding: 0,
            '&:last-child': {
                paddingBottom: 0,
            }
        }
    },
    MuiChip: {
        root: {
            height: 24,
            borderRadius: 12,
            fontWeight: '700',
            fontSize: 12,
            '&.chipLabel1': {
                backgroundColor: '#f0f2ff',
                color: '#7a97f4',
            },
            '&.chipLabel2': {
                backgroundColor: '#ecfaff',
                color: '#1fb6ff',
            },
            '&.chipLabel3': {
                backgroundColor: '#fff4ee',
                color: '#ff975c',
            },
            '&.chipLabel4': {
                backgroundColor: '#fff1f1',
                color: '#fe5050',
            },
            '&.chipLabel5': {
                backgroundColor: '#efefef',
                color: '#666',
            },
        },
        sizeSmall: {
            height: 20,
            borderRadius: 10,
        },
        outlined: {
            backgroundColor: '#fff !important',
            '&.chipLabel1': {
                borderColor: '#7a97f4',
            },
            '&.chipLabel2': {
                borderColor: '#1fb6ff',
            },
            '&.chipLabel3': {
                borderColor: '#ff975c',
            },
            '&.chipLabel4': {
                borderColor: '#fe5050',
            },
            '&.chipLabel5': {
                borderColor: '#666',
            },
        },
        label: {
            paddingLeft: 11,
            paddingRight: 11,
        },
        labelSmall: {
            paddingLeft: 7,
            paddingRight: 7,
        },
    },

    MuiInputAdornment: {
        positionEnd: {
            marginLeft: 0,
        },
    },






    MuiToolbar: {
        root: {
            borderBottom: '1px solid rgba(255, 255, 255, 0.06)',
        },
        regular: {
            minHeight: 50,
        },
        gutters: {
            paddingLeft: 50,
            paddingRight: 50,
        }
    },
}