import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import { DialogContent, DialogActions, TextField, InputAdornment, FormControlLabel, RadioGroup, Radio, Typography, Button, Tabs, Tab } from '@material-ui/core';
import { Header, ButtonClearInput, IconComplete } from '../../component';
/* import { Logo } from '../../static/images'; */

const useStyles = makeStyles((theme) => ({
    '@global': {
        '.formRow': {
            '& + .formRow, & + .boxRadioGroup': {
                marginTop: 10,
            }
        }
    },
    bgWhite: {
        margin: '0 -20px -55px',
        padding: '0 20px 55px',
        background: '#fff',
    },
    formFindId: {
        marginTop: 125,
    },
    resultFindId: {
        marginTop: 40,
    },
    formFindPw: {
        marginTop: 40,
        '& .mainFormGroup': {
            marginTop: 40,
        }
    },
    resultFindPw: {
        marginTop: 40,
    }
}));

const FindInfo = () => {
    const classes = useStyles();

    const [value, setValue] = React.useState(0);
    const [indicatorWidth, setIndicatorWidth] = React.useState(0);

    React.useEffect( () => {
        var target = document.querySelectorAll('.tabs .Mui-selected .tabLabelWrap')[0];
        var parent = target.closest('.tabs');
        var indicatorBar = parent.querySelector('#indicatorBar');

        var targetWidth = target.getBoundingClientRect().width;

        setIndicatorWidth(targetWidth);
    });

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    
    return (
        <React.Fragment>
            <Header isBack isWhite>아이디·비밀번호 찾기</Header>
            <div className="contents">
                <Tabs value={value} indicator="primary" variant="fullWidth" color="primary" aria-label="아이디·비밀번호 찾기" TabIndicatorProps={{ children: <span id="indicatorBar" className="bar" style={{ width : `${indicatorWidth}px`}}/> }} className="tabs" onChange={handleChange}>
                    <Tab classes={{ wrapper: 'tabLabelWrap'}} label="아이디 찾기"></Tab>
                    <Tab classes={{ wrapper: 'tabLabelWrap'}} label="비밀번호 찾기"></Tab>
                </Tabs>
                <div className={classes.formFindId}>
                    <div className="mainFormGroup">
                        <div className="formRow">
                            <TextField
                                id="userID"
                                /* error
                                helperText="아이디 오류" */
                                placeholder="아이디 입력"
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            <ButtonClearInput />
                                        </InputAdornment>
                                    ),
                                    classes: (
                                        {
                                            input: "input inputOutline",
                                            notchedOutline: "notchedOutline"
                                        }
                                    ),
                                    className: "inputBase"
                                }}
                                inputProps={
                                    { "title" : "아이디 입력" }
                                }
                                fullWidth
                                variant="outlined"
                                className={clsx('textField iconTextField typeIcoUserId sizeLg')}
                            />
                        </div>
                        <div className="formRow">
                            <TextField
                                type="tel"
                                id="certiNumber"
                                placeholder="등록된 휴대번호 입력"
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            <ButtonClearInput />
                                        </InputAdornment>
                                    ),
                                    classes: (
                                        {
                                            input: "input inputOutline",
                                            notchedOutline: "notchedOutline"
                                        }
                                    ),
                                    className: "inputBase"
                                }}
                                inputProps={
                                    { "title" : "등록된 휴대번호 입력" }
                                }
                                fullWidth
                                variant="outlined"
                                className={clsx('textField iconTextField typeIcoPhone sizeLg')}
                            />
                        </div>

                        <RadioGroup className="boxRadioGroup">
                            <FormControlLabel value="rdo1" className="boxRadioControl" classes={{label: 'boxRadioLabel'}} control={<Radio icon={""} checkedIcon={""} className="boxRadio" />} label="휴대폰으로 받기" />
                            <FormControlLabel value="rdo2" className="boxRadioControl" classes={{label: 'boxRadioLabel'}} control={<Radio icon={""} checkedIcon={""} className="boxRadio" />} label="이메일주소로 받기" />
                        </RadioGroup>
                    </div>
                    <div className="pageActions">
                        <Button variant="contained" size="large" fullWidth className="btnConfirm">확인</Button>
                    </div>
                </div>

                <div className={classes.resultFindId}>
                    <div className="completeMsgArea">
                        <IconComplete className="icoComplete" />
                        <Typography>등록하신 아이디는<br /> <Typography color="primary" component="strong" className="txtBold">datahubmaster</Typography> 입니다.</Typography>
                    </div>
                    <div className="pageActions">
                        <Button variant="contained" size="large" fullWidth className="btnConfirm">로그인</Button>
                    </div>
                </div>

                <div className={classes.formFindPw}>
                    <Typography>등록된 이메일 주소 또는 휴대폰번호로 임시 비밀번호를 발송해 드립니다.</Typography>
                    <div className="mainFormGroup">
                        <div className="formRow">
                            <TextField
                                id="userId"
                                placeholder="아이디 입력"
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            <ButtonClearInput />
                                        </InputAdornment>
                                    ),
                                    classes: (
                                        {
                                            input: "input inputOutline",
                                            notchedOutline: "notchedOutline"
                                        }
                                    ),
                                    className: "inputBase"
                                }}
                                inputProps={
                                    { "title" : "아이디 입력" }
                                }
                                fullWidth
                                variant="outlined"
                                className={clsx('textField iconTextField typeIcoUserId sizeLg')}
                            />
                        </div>

                        <RadioGroup className="boxRadioGroup">
                            <FormControlLabel value="rdo1" className="boxRadioControl" classes={{label: 'boxRadioLabel'}} control={<Radio icon={""} checkedIcon={""} className="boxRadio" />} label="휴대폰으로 받기" />
                            <FormControlLabel value="rdo2" className="boxRadioControl" classes={{label: 'boxRadioLabel'}} control={<Radio icon={""} checkedIcon={""} className="boxRadio" />} label="이메일주소로 받기" />
                        </RadioGroup>
                    </div>
                    <div className="pageActions">
                        <Button variant="contained" size="large" fullWidth className="btnConfirm">확인</Button>
                    </div>
                </div>

                <div className={classes.resultFindPw}>
                    <div className="completeMsgArea">
                        <IconComplete className="icoComplete" />
                        <Typography>이메일주소<br /> <Typography color="primary" component="strong" className="txtBold">gonggong**@naver.com</Typography>로<br />임시번호를 발송하였습니다.</Typography>
                    </div>
                    <div className="pageActions">
                        <Button variant="contained" size="large" fullWidth className="btnConfirm">로그인</Button>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
};

export default FindInfo;