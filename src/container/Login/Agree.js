import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Typography, DialogContent, DialogActions, IconButton, Button, FormControlLabel, Checkbox } from '@material-ui/core';
import { DialogTitle } from '../../component';
import ArrowForwardIosRoundedIcon from '@material-ui/icons/ArrowForwardIosRounded';

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: 35,
    },
    leadText: {
        marginBottom: 15,
    },
    agreeCheckboxes: {
        marginTop: 35,
        marginLeft: -20,
        marginRight: -20,
        borderTop: `1px solid ${theme.palette.divider}`,
        borderBottom: `1px solid ${theme.palette.divider}`,
        '&:before': {
            display: 'block',
            width: '100%',
            height: 6,
            backgroundColor: theme.palette.grey[200],
            content: '""',
        }
    },
    agreeLabelArea: {
        flex: 1,
    },
    agreeLabel: {
        width: '100%',
        marginLeft: 0,
        paddingTop: 3,
        paddingBottom: 3,
        paddingLeft: 11,
    },
    btnMore: {
        padding: 17,
    },
    icoMore: {
        width: 14,
        height: 14,
        color: theme.palette.grey[600],
    }
}));

const Alert = () => {
    const classes = useStyles();
    
    return (
        <React.Fragment>
            <DialogTitle isCancel>약관 재동의</DialogTitle>
            <DialogContent id="dialogContent">
                <div className={classes.root}>
                    <Typography className={clsx("sizeXLarge", classes.leadText)}>이용약관이 변경되어 재동의가 필요합니다.</Typography>
                    <Typography color="textSecondary">필수일 경우 동의하지 않으시면 서비스 이용에 제한을 받으실 수 있습니다.</Typography>
                    <div className={classes.agreeCheckboxes}>
                        <Grid container justify="space-between" alignItems="center" className={classes.agreeRow}>
                            <Grid item className={classes.agreeLabelArea}>
                                <FormControlLabel control={<Checkbox icon={<span className="icon"></span>} checkedIcon={<span className="icon iconChecked"></span>} className="sizeLarge rounded invert" />}  label="이용약관 동의(필수)" className={classes.agreeLabel} />
                            </Grid>
                            <Grid item>
                                <IconButton className={classes.btnMore}>
                                    <ArrowForwardIosRoundedIcon className={classes.icoMore} />
                                    <Typography variant="srOnly">이용약관 동의(필수) 보기</Typography>
                                </IconButton>
                            </Grid>
                        </Grid>
                    </div>
                </div>
            </DialogContent>
            <DialogActions disableSpacing>
                <Button variant="contained" className="btnConfirm">확인</Button>
            </DialogActions>
        </React.Fragment>
    );
};

export default Alert;