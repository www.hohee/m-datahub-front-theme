import React, { Component } from 'react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { ThemeProvider } from '@material-ui/styles';

import theme from './static/theme';
import View from './container/View';
import CssBaseline from '@material-ui/core/CssBaseline';
import './static/css/style.css';

const browserHistory = createBrowserHistory();

class App extends Component {
  
  render() {
    return (
      <ThemeProvider theme={theme}>
        <React.Fragment>
          <CssBaseline />
          {
            <Router history={browserHistory}>
              <View />
            </Router>
          }
        </React.Fragment>
      </ThemeProvider>
    );
  }
}

export default App;