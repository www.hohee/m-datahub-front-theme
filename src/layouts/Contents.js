import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { Drawer, IconButton, AppBar, Typography, Toolbar, Button, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import ArrowBackRoundedIcon from '@material-ui/icons/ArrowBackRounded';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Slide from '@material-ui/core/Slide';

function HideOnScroll(props) {
    const { children, window } = props;
    const trigger = useScrollTrigger({
        threshold: 100,
        target: window ? window() : undefined
    });

    return (
        <Slide appear={false} direction="down" in={!trigger}>
            {children}
        </Slide>
    );
}

const useStyles = makeStyles(theme => ({
    '@global': {
        
    },
    appbar: {
        backgroundColor: 'transparent',
        '&.on': {
            background: theme.palette.backgroundGradient,
        }
    },
    btnMenu: {
        position: 'absolute',
        left: 8,
    },
    btnMenuLabel: {
        display: 'block',
        '&:before': {
            display: 'block',
            width: 21,
            height: 2,
            marginBottom: 6,
            borderRadius: 1,
            backgroundColor: '#fff',
            content: '""',
        },
        '&:after': {
            display: 'block',
            width: 21,
            height: 2,
            marginTop: 6,
            borderRadius: 1,
            backgroundColor: '#fff',
            content: '""',
        },
        '& .icoMenu': {
            display: 'block',
            width: 16,
            height: 2,
            borderRadius: 1,
            backgroundColor: '#fff',
        },
    },
    pageHead: {
        flex: '1 1 auto',
        textAlign: 'center',
        color: '#fff',
    },
    container: {
        position: 'relative',
        minHeight: '100%',
        padding: '50px 20px 55px',
        background: theme.palette.grey[100],
        '&:before': {
            display: 'block',
            position: 'absolute',
            left: 0,
            top: 0,
            width: '100%',
            height: 50,
            background: theme.palette.backgroundGradient,
            content: '""',
        },
        '& .contents': {
            position: 'relative',
        },
        '& .stickyBottom': {
            position: 'fixed',
            left: 0,
            bottom: 0,
            width: '100%',
            '& .btnConfirm': {
                width: '100%',
                height: 55,
                borderLeft: '1px solid rgba(248, 249, 252, 0.1)',
                borderRadius: 0,
                fontWeight: '700',
                '&:first-child': {
                    borderLeft: 'none',
                }
            },
        }
    }
}));

const Default = props => {
    const classes = useStyles();

    const { children } = props;

    const [ state, setState ] = React.useState(false);

    window.addEventListener('scroll', function(){
        setState( window.pageYOffset > 0 ? true : false );
    });

    return (
        <React.Fragment>
            <HideOnScroll {...props}>
                <AppBar elevation={0} className={clsx(classes.appbar, {'on': state})}>
                    <Toolbar>
                        <IconButton aria-label="메뉴" classes={{ label: classes.btnMenuLabel }} className={classes.btnMenu}>
                            <span className='icoMenu'></span>
                        </IconButton>
                        <Typography variant="h1" className={classes.pageHead}>My Page</Typography>
                    </Toolbar>
                </AppBar>
            </HideOnScroll>
            <div className={classes.container}>
                {children}
            </div>
        </React.Fragment>
    );
};

Default.propTypes = {
    children: PropTypes.node
};

export default Default;