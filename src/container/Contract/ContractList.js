import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import { Link, Grid, Card, CardContent, IconButton, Typography, Button, Chip, TextField } from '@material-ui/core';
import AddRoundedIcon from '@material-ui/icons/AddRounded';
import { Header, SortingPopup } from '../../component';
import { IcoMyPage, IcoSorting, IcoSearch, IcoArrowDownWhite } from '../../static/images';

const useStyles = makeStyles((theme) => ({
    intro: {
        position: 'relative',
        top: -51,
        margin: '0 -20px -85px',
        padding: '50px 0 75px',
        background: theme.palette.backgroundGradient,
    },
    sortArea: {
        justifyContent: 'flex-start',
        padding: 0,
        borderBottom: '1px solid rgba(255, 255, 255, 0.06)',
        backgroundColor: 'rgba(255, 255, 255, 0.06)',
        textAlign: 'left',
        '&:after': {
            display: 'block',
            width: 45,
            height: 44,
            borderLeft: '1px solid rgba(255, 255, 255, 0.06)',
            background: `url(${IcoSearch}) no-repeat center`,
            backgroundSize: '15px auto',
            content: '""',
        },
    },
    sort: {
        display: 'inline-flex',
        alignItems: 'center',
        position: 'relative',
        width: '100%',
        height: 44,
        padding: '0 20px 0 44px',
        borderLeft: '1px solid rgba(255, 255, 255, 0.06)',
        color: '#fff',
        '&:first-child': {
            borderLeft: 'none',
        },
        '&:before, &:after': {
            display: 'block',
            position: 'absolute',
            top: '50%',
            transform: 'translateY(-50%)',
            content: '""',
        },
        '&:before': {
            left: 20,
            width: 16,
            height: 16,
            background: `url(${IcoSorting}) no-repeat 0 0`,
            backgroundSize: '42px auto',
        },
        '&:after': {
            right: 15,
            width: 11,
            height: 6,
            background: `url(${IcoArrowDownWhite}) no-repeat 0 0`,
            backgroundSize: '11px auto',
        },
    },
    sortStat: {
        '&:before': {
            backgroundPosition: '-26px 0',
        }
    },

    /* sortTest: {
        justifyContent: 'flex-end',
        paddingRight: 50,
        fontSize: 12,
        '&:before': {
            background: `url(${IcoSearch}) no-repeat center`,
            backgroundSize: '15px auto',
        },
        '&:after': {
            right: 20,
        }
    } */
}));

const ContractList = () => {
    const classes = useStyles();

    return (
        <React.Fragment>
            <Header>신계약조회</Header>
            <div className="contents">
                <div className={classes.intro}>
                    <Button fullWidth aria-label="조회조건 설정" className={classes.sortArea}>
                        <span className={clsx(classes.sort, classes.sortDate)}>조회기간</span>
                        <span className={clsx(classes.sort, classes.sortStat)}>처리상태</span>
                    </Button>
                    {/* <Button fullWidth aria-label="조회조건 설정" className={classes.sortArea}>
                        <span className={clsx(classes.sort, classes.sortTest)}>조회기간 · 처리상태</span>
                    </Button> */}
                </div>
                <Button className="contractCard">
                    <div className="infoGroup">
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">이*섭 / 홍*주</Grid>
                            <Grid item className="infoDesc"><Chip label="승인요청" className="chipLabel1" /></Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">동양생명</Grid>
                            <Grid item className="infoDesc">00000000000000</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">월납</Grid>
                            <Grid item className="infoDesc"><strong>126,000 원</strong></Grid>
                        </Grid>
                    </div>
                </Button>
                <Button className="contractCard">
                    <div className="infoGroup">
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">이*섭 / 홍*주</Grid>
                            <Grid item className="infoDesc"><Chip label="승인요청" className="chipLabel1" /></Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">동양생명</Grid>
                            <Grid item className="infoDesc">00000000000000</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">월납</Grid>
                            <Grid item className="infoDesc"><strong>126,000 원</strong></Grid>
                        </Grid>
                    </div>
                </Button>
                <Button className="contractCard">
                    <div className="infoGroup">
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">이*섭 / 홍*주</Grid>
                            <Grid item className="infoDesc"><Chip label="승인요청" className="chipLabel1" /></Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">동양생명</Grid>
                            <Grid item className="infoDesc">00000000000000</Grid>
                        </Grid>
                        <Grid container justify="space-between" className="row">
                            <Grid item className="infoSubhead">월납</Grid>
                            <Grid item className="infoDesc"><strong>126,000 원</strong></Grid>
                        </Grid>
                    </div>
                </Button>

                <Button fullWidth className="btnMore" startIcon={<AddRoundedIcon />} style={{marginTop: 4,}}>더보기</Button>

                <SortingPopup />
            </div>
            {/* <div className="stickyBottom">
                <Button variant="contained" fullWidth className="btnConfirm">버튼</Button>
            </div> */}
        </React.Fragment>
    );
};

export default ContractList;