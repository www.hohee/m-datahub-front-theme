import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Typography, Button, TextField, InputAdornment } from '@material-ui/core';
import { Header, ButtonClearInput } from '../../component';
import { IcoMyPage, IcoEdit } from '../../static/images';

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: 20,
    },
    formArea: {
        marginTop: 15,
        '& .formRow + .formRow': {
            marginTop: 10,
        }
    },
}));

const EditInfo = () => {
    const classes = useStyles();

    return (
        <React.Fragment>
            <Header isBack>기본정보수정</Header>
            <div className="contents">
                <div className={classes.root}>
                    <Typography component="h2">기본정보</Typography>
                    <div className={classes.formArea}>
                        <div className="formRow">
                            <Button className="fakeInput" fullWidth aria-label="휴대폰번호 수정">010-****-5678</Button>
                        </div>
                        <div className="formRow">
                            <TextField
                                type="email"
                                id="userEmail"
                                defaultValue="gongong@globalfm.com"
                                /* error
                                helperText="이메일 오류" */
                                placeholder="이메일 입력"
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            <ButtonClearInput />
                                        </InputAdornment>
                                    ),
                                    classes: (
                                        {
                                            input: "input inputOutline",
                                            notchedOutline: "notchedOutline"
                                        }
                                    ),
                                    className: "inputBase"
                                }}
                                inputProps={
                                    { "title" : "이메일 입력" }
                                }
                                fullWidth
                                variant="outlined"
                                className={clsx('textField iconTextField typeIcoEmail sizeLg')}
                            />
                        </div>
                    </div>
                </div>
            </div>
            <div className="stickyBottom">
                <Button variant="contained" fullWidth className="btnConfirm">수정하기</Button>
            </div>
        </React.Fragment>
    );
};

export default EditInfo;