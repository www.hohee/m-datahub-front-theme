import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Typography, Button, TextField, InputAdornment, DialogContent, DialogActions } from '@material-ui/core';
import { DialogTitle, ButtonClearInput } from '../../component';
import { IcoMyPage, IcoEdit } from '../../static/images';

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: 20,
        '& .formGroup': {
            marginTop: 15,
        }
    },
}));

const EditPhone = () => {
    const classes = useStyles();

    return (
        <React.Fragment>
            <DialogTitle>휴대폰번호 변경</DialogTitle>
            <DialogContent id="dialogContent">
                <div className={classes.root}>
                    <Typography component="h2">휴대폰번호</Typography>
                    <div className="formGroup">
                        <TextField
                            type="tel"
                            id="userPhone"
                            defaultValue="010-3456-6789"
                            placeholder="휴대폰번호 입력"
                            InputProps={{
                                endAdornment: (
                                    <InputAdornment position="end">
                                        <ButtonClearInput />
                                        <Button variant="contained" size="small" className="btnInForm">인증요청</Button>
                                    </InputAdornment>
                                ),
                                classes: (
                                    {
                                        input: "input inputOutline",
                                        notchedOutline: "notchedOutline"
                                    }
                                ),
                                className: "inputBase"
                            }}
                            inputProps={
                                { "title" : "휴대폰번호 입력" }
                            }
                            fullWidth
                            variant="outlined"
                            className={clsx('textField iconTextField typeIcoPhone sizeLg')}
                        />
                        <TextField
                            id="certiNumber"
                            type="tel"
                            /* error
                            helperText="인증번호 오류" */
                            placeholder="인증번호 6자리 입력"
                            InputProps={{
                                endAdornment: (
                                    <InputAdornment position="end">
                                        <ButtonClearInput />
                                        <Typography color="error" className="timeLimit"><Typography variant="srOnly">인증번호 유효시간</Typography>5:00</Typography>
                                    </InputAdornment>
                                ),
                                classes: (
                                    {
                                        input: "input inputOutline",
                                        notchedOutline: "notchedOutline"
                                    }
                                ),
                                className: "inputBase"
                            }}
                            inputProps={
                                { "title" : "인증번호 여섯자리 입력", "maxLength" : "6" }
                            }
                            fullWidth
                            variant="outlined"
                            className={clsx('textField iconTextField typeIcoCheck sizeLg')}
                        />
                    </div>
                </div>
            </DialogContent>
            <DialogActions disableSpacing>
                <Button variant="contained" className="btnConfirm" disabled>확인</Button>
            </DialogActions>
        </React.Fragment>
    );
};

export default EditPhone;