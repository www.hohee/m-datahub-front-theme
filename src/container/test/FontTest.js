import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Link, Grid, Card, CardHeader, CardContent, CardActions, IconButton, Typography, Button, Chip } from '@material-ui/core';
import IconEdit from '../../static/images/ico_edit.png';

const useStyles = makeStyles((theme) => ({
    myInfoArea: {
        height: 290,
        marginLeft: -20,
        marginRight: -20,
        marginTop: -50,
        paddingTop: 94,
        paddingLeft: 38,
        paddingRight: 38,
        background: theme.palette.backgroundGradient,
    },
    welcome: {
        marginBottom: 18,
    },
    userName: {
        fontWeight: '400',
        fontSize: 32,
        color: '#fff',
    },
    userStatusChip: {
        height: 20,
        marginLeft: 6,
        border: '1px solid #a0e1ff',
        fontSize: 11,
        color: '#a0e1ff',
    },
    myInfo: {
        marginTop: 5,
    },
    myInfoTit: {
        flexBasis: 95,
        color: '#fff',
        opacity: 0.8,
    },
    myInfoDesc: {
        color: '#fff'
    },
    cardGroup: {
        marginTop: -30,
    },
    card: {
        position: 'relative',
        marginTop: 15,
        paddingTop: 22,
        paddingRight: 18,
        paddingBottom: 22,
        paddingLeft: 18,
        borderRadius: 6,
        '&:first-child': {
            marginTop: 0,
        }
    },
    cardHeader: {
        paddingBottom: 20,
        fontWeight: '700',
        fontSize: 15,
    },
    infoList: {
        marginTop: 8,
        '&:first-child': {
            marginTop: 0,
        }
    },
    infoTit: {
        //flexBasis: 115,
        paddingRight: 10,
        color: '#757f95'
    },
    infoDesc: {
        flex: 1,
        textAlign: 'right',
        wordBreak: 'break-word',
        color: '#333'
    },
    cardActions: {
        padding: 0,
    },
    btnEdit: {
        position: 'absolute',
        right: 20,
        top: 15,
        width: 34,
        height: 34,
        background: `#f3f4f5 url(${IconEdit}) no-repeat center`,
        backgroundSize: 13,
    },
}));

const FontTest = () => {
    const classes = useStyles();

    return (
        <React.Fragment>
            <div className="contents">
                <div className={classes.myInfoArea}>
                    <div className={classes.welcome}><Typography component="strong" className={classes.userName}><span>공지철</span>님</Typography> <Chip label="정상" variant="outlined" size="small" className={classes.userStatusChip} /></div>
                    <Grid container className={classes.myInfo}>
                        <Grid item className={classes.myInfoTit}>사번</Grid>
                        <Grid item className={classes.myInfoDesc}>0000000000</Grid>
                    </Grid>
                    <Grid container className={classes.myInfo}>
                        <Grid item className={classes.myInfoTit}>아이디</Grid>
                        <Grid item className={classes.myInfoDesc}>ggonggong</Grid>
                    </Grid>
                    <Grid container className={classes.myInfo}>
                        <Grid item className={classes.myInfoTit}>사용등록일</Grid>
                        <Grid item className={classes.myInfoDesc}>2019.03.01</Grid>
                    </Grid>
                </div>
                <div className={classes.cardGroup}>
                    <Card elevation={0} className={classes.card}>
                        <CardHeader
                            title="기본정보"
                            classes={{ title: classes.cardHeader }}
                        />
                        <CardContent>
                            <Grid container direction="row" justify="space-between" className={classes.infoList}>
                                <Grid item className={classes.infoTit}>이름</Grid>
                                <Grid item className={classes.infoDesc}>공지철</Grid>
                            </Grid>
                            <Grid container direction="row" justify="space-between" className={classes.infoList}>
                                <Grid item className={classes.infoTit}>휴대폰번호</Grid>
                                <Grid item className={classes.infoDesc}>010-****-5678</Grid>
                            </Grid>
                            <Grid container direction="row" justify="space-between" className={classes.infoList}>
                                <Grid item className={classes.infoTit}>이메일주소</Grid>
                                <Grid item className={classes.infoDesc}>gonggong@globalfm.com</Grid>
                            </Grid>
                            <Grid container direction="row" justify="space-between" className={classes.infoList}>
                                <Grid item className={classes.infoTit}>생년월일</Grid>
                                <Grid item className={classes.infoDesc}>701010-1*******</Grid>
                            </Grid>
                        </CardContent>
                        <CardActions disableSpacing className={classes.cardActions}>
                            <IconButton className={classes.btnEdit}><Typography variant="srOnly">기본정보수정</Typography></IconButton>
                        </CardActions>
                    </Card>
                    <Card elevation={0} className={classes.card}>
                        <CardHeader
                            title="소속정보"
                            classes={{ title: classes.cardHeader }}
                        />
                        <CardContent>
                            <Grid container direction="row" justify="space-between" className={classes.infoList}>
                                <Grid item className={classes.infoTit}>GA명</Grid>
                                <Grid item className={classes.infoDesc}>글로벌 금융판매</Grid>
                            </Grid>
                            <Grid container direction="row" justify="space-between" className={classes.infoList}>
                                <Grid item className={classes.infoTit}>소속지점명</Grid>
                                <Grid item className={classes.infoDesc}>S&amp;P &gt; 강남지사 &gt; 신사동지점</Grid>
                            </Grid>
                            <Grid container direction="row" justify="space-between" className={classes.infoList}>
                                <Grid item className={classes.infoTit}>협회코드</Grid>
                                <Grid item className={classes.infoDesc}>YYYY0000000000000</Grid>
                            </Grid>
                            <Grid container direction="row" justify="space-between" className={classes.infoList}>
                                <Grid item className={classes.infoTit}>영업구분</Grid>
                                <Grid item className={classes.infoDesc}>순수내근</Grid>
                            </Grid>
                            <Grid container direction="row" justify="space-between" className={classes.infoList}>
                                <Grid item className={classes.infoTit}>직책</Grid>
                                <Grid item className={classes.infoDesc}>설계사</Grid>
                            </Grid>
                            <Grid container direction="row" justify="space-between" className={classes.infoList}>
                                <Grid item className={classes.infoTit}>직급</Grid>
                                <Grid item className={classes.infoDesc}>과장</Grid>
                            </Grid>
                            <Grid container direction="row" justify="space-between" className={classes.infoList}>
                                <Grid item className={classes.infoTit}>자격</Grid>
                                <Grid item className={classes.infoDesc}>명인</Grid>
                            </Grid>
                            <Grid container direction="row" justify="space-between" className={classes.infoList}>
                                <Grid item className={classes.infoTit}>영업속성</Grid>
                                <Grid item className={classes.infoDesc}>라운지</Grid>
                            </Grid>
                            <Grid container direction="row" justify="space-between" className={classes.infoList}>
                                <Grid item className={classes.infoTit}>소득구분</Grid>
                                <Grid item className={classes.infoDesc}>근로</Grid>
                            </Grid>
                            <Grid container direction="row" justify="space-between" className={classes.infoList}>
                                <Grid item className={classes.infoTit}>유치자코드</Grid>
                                <Grid item className={classes.infoDesc}></Grid>
                            </Grid>
                            <Grid container direction="row" justify="space-between" className={classes.infoList}>
                                <Grid item className={classes.infoTit}>위촉/해촉</Grid>
                                <Grid item className={classes.infoDesc}>위촉</Grid>
                            </Grid>
                            <Grid container direction="row" justify="space-between" className={classes.infoList}>
                                <Grid item className={classes.infoTit}>재직구분</Grid>
                                <Grid item className={classes.infoDesc}>재직</Grid>
                            </Grid>
                            <Grid container direction="row" justify="space-between" className={classes.infoList}>
                                <Grid item className={classes.infoTit}>위촉일</Grid>
                                <Grid item className={classes.infoDesc}>2011-11-11</Grid>
                            </Grid>
                            <Grid container direction="row" justify="space-between" className={classes.infoList}>
                                <Grid item className={classes.infoTit}>해촉일</Grid>
                                <Grid item className={classes.infoDesc}></Grid>
                            </Grid>
                        </CardContent>
                    </Card>
                    <Card elevation={0} className={classes.card}>
                        <CardHeader
                            title="설계사 추가정보"
                            classes={{ title: classes.cardHeader }}
                        />
                        <CardContent>
                            <Grid container direction="row" justify="space-between" className={classes.infoList}>
                                <Grid item className={classes.infoTit}>우수인증설계사</Grid>
                                <Grid item className={classes.infoDesc}>Y</Grid>
                            </Grid>
                            <Grid container direction="row" justify="space-between" className={classes.infoList}>
                                <Grid item className={classes.infoTit}>모집경력시스템동의</Grid>
                                <Grid item className={classes.infoDesc}>Y</Grid>
                            </Grid>
                            <Grid container direction="row" justify="space-between" className={classes.infoList}>
                                <Grid item className={classes.infoTit}>변액자격여부</Grid>
                                <Grid item className={classes.infoDesc}>Y</Grid>
                            </Grid>
                            <Grid container direction="row" justify="space-between" className={classes.infoList}>
                                <Grid item className={classes.infoTit}>변액자격취득일</Grid>
                                <Grid item className={classes.infoDesc}>2010-08-10</Grid>
                            </Grid>
                        </CardContent>
                    </Card>
                    <Card elevation={0} className={classes.card}>
                        <CardHeader
                            title="마케팅 동의"
                            classes={{ title: classes.cardHeader }}
                        />
                        <CardContent>
                            <Grid container direction="row" justify="space-between" className={classes.infoList}>
                                <Grid item className={classes.infoTit}>마케팅동의버전</Grid>
                                <Grid item className={classes.infoDesc}>V 1.0</Grid>
                            </Grid>
                            <Grid container direction="row" justify="space-between" className={classes.infoList}>
                                <Grid item className={classes.infoTit}>마케팅동의 종료일</Grid>
                                <Grid item className={classes.infoDesc}>2020-10-10</Grid>
                            </Grid>
                        </CardContent>
                    </Card>
                </div>
                <div className="pageActions">
                    <Button variant="contained" color="primary" fullWidth>보험사코드 보기</Button>
                </div>
            </div>
        </React.Fragment>
    );
};

export default FontTest;