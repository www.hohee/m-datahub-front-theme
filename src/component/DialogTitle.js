import React from 'react';
import { IconButton, Typography, Button } from '@material-ui/core';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import { BtnClearInput, BtnClosePopup } from '../static/images';

const style = theme => ({
    root: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        minHeight: 50,
        padding: '10px 55px',
        borderBottom: `1px solid ${theme.palette.grey[300]}`,
    },
    btnClose: {
        position: 'absolute',
        left: 5,
        top: 2,
        width: 45,
        height: 45,
        background: `url(${BtnClosePopup}) no-repeat center`,
        backgroundSize: 15,
    },
    btnText: {
        position: 'absolute',
        right: 1,
        top: 6,
        fontSize: 14,
        color: '#666',
    },
});

const DialogTitle = withStyles(style)(props => {
    const { children, classes, isCancel, ...other } = props;
    
    return (
        <MuiDialogTitle disableTypography className={classes.root} {...other}>
            <Typography variant="h1">{children}</Typography>
            {
                isCancel ? (
                    <Button className={classes.btnText}>취소</Button>
                ) : (
                    <IconButton aria-label="닫기" className={classes.btnClose}></IconButton>
                )
            }
        </MuiDialogTitle>
    );
});


const Default = props => {

    const { isCancel, children } = props;

    return (
        <DialogTitle isCancel={ isCancel ? true : false } id="dialogTitle">{children}</DialogTitle>
    );
};

export default Default;