import React from 'react';
import clsx from 'clsx';
import { IconButton, Typography, AppBar, Toolbar } from '@material-ui/core';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import { makeStyles } from '@material-ui/core/styles';
import { BtnClearInput } from '../static/images';
import ArrowBackIosRoundedIcon from '@material-ui/icons/ArrowBackIosRounded';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Slide from '@material-ui/core/Slide';

function HideOnScroll(props) {
    const { children, window } = props;
    const trigger = useScrollTrigger({
        threshold: 100,
        target: window ? window() : undefined
    });

    return (
        <Slide appear={false} direction="down" in={!trigger}>
            {children}
        </Slide>
    );
}

const useStyles = makeStyles(theme => ({
    appbar: {
        height: 50,
        borderBottom: '1px solid rgba(255, 255, 255, 0.06)',
        backgroundColor: 'transparent',
        '&.white': {
            borderBottom: `1px solid ${theme.palette.grey[300]}`,
            backgroundColor: '#fff',
        },
        '&:not(.white)': {
            '&.on': {
                background: theme.palette.backgroundGradient,
            },
            '& .pageHead': {
                color: '#fff',
            },
        },
        '& .pageHead': {
            flex: '1 1 auto',
            textAlign: 'center',
        },
    },
    toolbar: {
        display: 'flex',
        position: 'relative',
        justifyContent: 'center',
        alignItems: 'center',
        minHeight: 50,
        padding: '10px 55px',
        border: 'none',
    },
    btnMenu: {
        position: 'absolute',
        left: 8,
    },
    btnMenuLabel: {
        display: 'block',
        '&:before': {
            display: 'block',
            width: 21,
            height: 2,
            marginBottom: 6,
            borderRadius: 1,
            backgroundColor: '#fff',
            content: '""',
        },
        '&:after': {
            display: 'block',
            width: 21,
            height: 2,
            marginTop: 6,
            borderRadius: 1,
            backgroundColor: '#fff',
            content: '""',
        },
        '& .icoMenu': {
            display: 'block',
            width: 16,
            height: 2,
            borderRadius: 1,
            backgroundColor: '#fff',
        },
    },
    btnBack: {
        position: 'absolute',
        left: 3,
        top: '50%',
        transform: 'translateY(-50%)',
    },
    btnBackIcon: {
        width: 20,
        height: 20,
        color: '#fff',
        '&.white': {
            color: '#7f7f7f',
        },
    }
}));

const Header = props => {
    const classes = useStyles();

    const { isBack, isWhite, className, children } = props;

    const [ state, setState ] = React.useState(false);

    window.addEventListener('scroll', function(){
        setState( window.pageYOffset > 0 ? true : false );
    });

    return (
        <HideOnScroll {...props}>
            <AppBar elevation={0} className={clsx(classes.appbar, {'on': state, 'white': isWhite}, className)}>
                <Toolbar className={classes.toolbar}>
                    {
                        isBack ? (
                            <React.Fragment>
                                <Typography variant="h1" className="pageHead">{children}</Typography>
                                <IconButton aria-label="뒤로가기" className={classes.btnBack}>
                                    <ArrowBackIosRoundedIcon className={clsx(classes.btnBackIcon, {'white': isWhite})} />
                                </IconButton>
                            </React.Fragment>
                        ) : (
                            <React.Fragment>
                                <IconButton aria-label="메뉴" classes={{ label: classes.btnMenuLabel }} className={classes.btnMenu}>
                                    <span className='icoMenu'></span>
                                </IconButton>
                                <Typography variant="h1" className="pageHead">{children}</Typography>
                            </React.Fragment>
                        )
                    }
                </Toolbar>
            </AppBar>
        </HideOnScroll>
    );
};

export default Header;