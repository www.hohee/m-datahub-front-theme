import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import { Link, Grid, Card, CardContent, IconButton, Typography, Button, Chip } from '@material-ui/core';
import { Header } from '../../component';
import { IcoMyPage } from '../../static/images';

const useStyles = makeStyles((theme) => ({
    intro: {
        position: 'relative',
        top: -50,
        margin: '0 -20px -85px',
        padding: '50px 20px 75px',
        background: theme.palette.backgroundGradient,
    },
    myInfoWrap: {
        position: 'relative',
        padding: '40px 20px 0',
    },
    welcomeMsg: {
        fontWeight: '300',
        fontSize: 30,
        lineHeight: 1.4,
        color: '#fff',
    },
    myGroup: {
        position: 'relative',
        marginTop: 25,
        paddingLeft: 11,
        color: '#fff',
        opacity: 0.6,
        '&:before': {
            display: 'block',
            position: 'absolute',
            left: 0,
            top: 9,
            width: 3,
            height: 3,
            borderRadius: '50%',
            backgroundColor: '#fff',
            opacity: 0.8,
            content: '""',
        },
        '& > span': {
            display: 'inline-flex',
            alignItems: 'center',
            '&:after': {
                marginLeft: 9,
                marginRight: 9,
                fontSize: 11,
                content: '">"',
            },
            '&:last-child:after': {
                display: 'none',
            }
        },
    },
    editMyInfo: {
        position: 'absolute',
        right: 0,
        top: 20,
        padding: '0 0 0 17px',
        fontSize: 12,
        color: '#fff',
        opacity: 0.8,
        '&:before': {
            display: 'block',
            position: 'absolute',
            left: 0,
            top: 5,
            width: 11,
            height: 11,
            background: `url(${IcoMyPage}) no-repeat 0 0`,
            backgroundSize: '150px',
            content: '""',
        }
    },
    remindWrap: {
        marginTop: 25,
    },
    remind: {
        position: 'relative',
        marginTop: 15,
        paddingLeft: 60,
        color: '#fff',
        '&:before': {
            display: 'block',
            position: 'absolute',
            left: 0,
            width: 42,
            height: 42,
            borderRadius: '50%',
            background: `rgba(51, 82, 232, 0.15) url(${IcoMyPage}) no-repeat 0 -21px`,
            backgroundSize: '150px',
            content: '""',
        },
        '& dd': {
            opacity: 0.7,
            '& > span': {
                marginRight: 15,
            }
        },
    },
    remindBirthday: {
        '&:before': {
            backgroundPosition: '0 -21px',
        }
    },
    remindRenewal: {
        '&:before': {
            backgroundPosition: '-52px -21px',
        }
    },
}));

const MyPageMain = () => {
    const classes = useStyles();

    return (
        <React.Fragment>
            <Header>My Page</Header>
            <div className="contents">
                <div className={classes.intro}>
                    <div className={classes.myInfoWrap}>
                        <Typography className={classes.welcomeMsg}>공지철님<br /> 반갑습니다 :D</Typography>
                        <div className={classes.myGroup}><span>중앙본부</span><span>중앙지사</span><span>중앙지점</span></div>
                        <Button className={classes.editMyInfo}>내정보 관리</Button>
                    </div>
                    <div className={classes.remindWrap}>
                        <dl className={clsx(classes.remind, classes.remindBirthday)}>
                            <dt><strong>생일</strong> 맞으신 고객님</dt>
                            <dd><span>박보검</span><span>이동욱</span></dd>
                        </dl>
                        <dl className={clsx(classes.remind, classes.remindRenewal)}>
                            <dt><strong>보험갱신</strong> 필요한 고객님</dt>
                            <dd><span>박보검</span><span>이동욱</span></dd>
                        </dl>
                    </div>
                </div>
                <Card elevation={0} className="contractCard hasIcon icoNewContract">
                    <Typography variant="h2" className="contractCardHead">진행중인 신계약</Typography>
                    <CardContent>
                        <div className="contractCardSubdesc">기준일 2019-12-11</div>
                        <div className="contractCardDesc"><strong className="value">3</strong> 건</div>
                        <div className="contractCardDesc"><strong className="value">230,000</strong> 원</div>
                    </CardContent>
                </Card>
                <Card elevation={0} className="contractCard hasIcon">
                    <Typography variant="h2" className="contractCardHead">보유 계약</Typography>
                    <CardContent>
                        <div className="contractCardSubdesc">기준일 2019-12-11</div>
                        <div className="contractCardDesc"><strong className="value">236</strong> 건</div>
                        <div className="contractCardDesc"><strong className="value">37,562,000</strong> 원</div>
                    </CardContent>
                    <CardContent className="contractCardDetail">
                        <div className="row">
                            <Chip size="small" variant="outlined" label="정상" className="chipLabel2" />
                            <strong>96</strong> 건<span className="blank">/</span><strong>27,562,000</strong> 원
                        </div>
                        <div className="row">
                            <Chip size="small" variant="outlined" label="연체" className="chipLabel3" />
                            <strong>96</strong> 건<span className="blank">/</span><strong>27,562,000</strong> 원
                        </div>
                        <div className="row">
                            <Chip size="small" variant="outlined" label="실효" className="chipLabel4" />
                            <strong>96</strong> 건<span className="blank">/</span><strong>27,562,000</strong> 원
                        </div>
                    </CardContent>
                </Card>
                <div className="pageActions">
                    <Button variant="contained" color="primary" fullWidth>보험사코드 보기</Button>
                </div>
            </div>
            {/* <div className="stickyBottom">
                <Button variant="contained" fullWidth className="btnConfirm">버튼</Button>
            </div> */}
        </React.Fragment>
    );
};

export default MyPageMain;