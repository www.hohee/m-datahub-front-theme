FROM nginx:alpine
COPY nginx/nginx.conf /etc/nginx/nginx.conf
COPY build /m-datahub-front-theme
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
