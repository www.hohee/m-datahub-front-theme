import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { Drawer } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { Sidebar, Footer, Topbar } from './components';
import './guide.css';

const drawerWidth = 240;
const useStyles = makeStyles(theme => ({
    '@global': {
        '.guideTitle': {
            marginBottom: 25,
            fontSize: 28,
            '& span': {
                display: 'inline-block',
                position: 'relative',
                padding: '0 3px',
                '&:after': {
                    display: 'block',
                    position: 'absolute',
                    bottom: 0,
                    left: 0,
                    right: 0,
                    height: 10,
                    borderRadius: 3,
                    background: 'rgba(253, 197, 218, 0.5)',
                    zIndex: -1,
                    content: '""',
                }
            }
        },
        '.guide + .guide': {
            marginTop: 20,
        }
    },
    contentBody: {
        display: 'block',
        margin: '100px 40px 45px',
    },
}));

const Guide = props => {
    const { children } = props;

    const classes = useStyles();
    
    return (
        <div className={classes.wrap}>
            <Topbar />
            <main className={clsx(classes.contentBody, "guideContents")}>{children}</main>
        </div>
    );
}

Guide.propTypes = {
    children: PropTypes.node
};

export default Guide;