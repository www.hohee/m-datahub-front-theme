import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/styles';
import { AppBar, Toolbar } from '@material-ui/core';
import LogoCNS from '../../../../static/images/logo_cns.png';

const useStyles = makeStyles(theme => ({
    header: {
        display: 'block',
        paddingLeft: 40,
        paddingRight: 40,
        background: 'linear-gradient(190deg, rgba(114,145,243,1) 0%, rgba(193,108,243,1) 100%)',
    },
    globalNav: {
        textAlign: 'center',
        '& .MuiListItemText-primary': {
            fontWeight: '700',
            fontSize: 15,
            color: '#fff',
            opacity: 0.7,
        },
        '& button': {
            paddingLeft: 15,
            paddingRight: 15,
            borderRadius: 4,
        },
        '&.globalNavOn': {
            '& .MuiListItemText-primary': {
                opacity: 1,
            },
        },
    },
}));

const Topbar = props => {
    const classes = useStyles();
    
    return (
        <AppBar elevation={1} className={classes.header}>
            <Toolbar disableGutters>
                <RouterLink to="/">
                    <img src={LogoCNS} alt="LG CNS" />
                </RouterLink>
            </Toolbar>
        </AppBar>
    );
};

export default Topbar;