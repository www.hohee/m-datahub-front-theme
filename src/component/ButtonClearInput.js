import React from 'react';
import { IconButton, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { BtnClearInput } from '../static/images';

const useStyles = makeStyles((theme) => ({
    root: {
        padding: 13,
    },
    icoClear: {
        width: 20,
        height: 20,
        background: `url(${BtnClearInput}) no-repeat center`,
        backgroundSize: 'contain',
    },
}));


const ButtonClearInput = () => {
    const classes = useStyles();

    return (
        <IconButton className={classes.root}><span className={classes.icoClear}><Typography variant="srOnly">입력값 삭제</Typography></span></IconButton>
    );
};

export default ButtonClearInput;