import React, { Component } from 'react';
import { Switch } from 'react-router-dom';
import RouteWithLayout from '../component/RouteWithLayout/RouteWithLayout';
import { Guide as GuideL, ContainerWhite as ContainerWhiteL, Container as ContainerL, FullPopup as FullPopupLayout } from '../layouts';
import {
    Alert,
    Login, Find, Reset, Agree,
    MyPageMain, MyInfo, EditInfo, EditPhone,
    ContractList, ContractDetail,
    PublishingList, FontTest, Test
} from '../container'

class View extends Component {
  render() {
    return (
      <Switch>

        <RouteWithLayout exact path="/" layout={GuideL} component={PublishingList} />

        <RouteWithLayout exact path="/alert" layout={ContainerL} component={Alert} />

        <RouteWithLayout exact path="/login" layout={ContainerWhiteL} component={Login} />
        <RouteWithLayout exact path="/reset" layout={FullPopupLayout} component={Reset} />
        <RouteWithLayout exact path="/find" layout={ContainerWhiteL} component={Find} />
        <RouteWithLayout exact path="/agree" layout={FullPopupLayout} component={Agree} />

        <RouteWithLayout exact path="/mypage" layout={ContainerL} component={MyPageMain} />
        <RouteWithLayout exact path="/myinfo" layout={ContainerL} component={MyInfo} />
        <RouteWithLayout exact path="/edit" layout={ContainerL} component={EditInfo} />
        <RouteWithLayout exact path="/phone" layout={FullPopupLayout} component={EditPhone} />

        <RouteWithLayout exact path="/contractList" layout={ContainerL} component={ContractList} />
        <RouteWithLayout exact path="/contractDetail" layout={ContainerL} component={ContractDetail} />

        <RouteWithLayout exact path="/test" layout={ContainerL} component={FontTest} />
      </Switch>
    );
  }
}

export default View;