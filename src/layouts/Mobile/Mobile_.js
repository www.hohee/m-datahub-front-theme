import React from 'react';
import PropTypes from 'prop-types';
import { Drawer, IconButton, AppBar, Typography, Toolbar, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import MenuRoundedIcon from '@material-ui/icons/MenuRounded';
import CloseRoundedIcon from '@material-ui/icons/CloseRounded';

const useStyles = makeStyles(theme => ({
    menuButton: {
        marginLeft: 0,
    },
    pageHead: {
        margin: 0,
        textAlign: 'center',
    },
    navigation: {
        width: '100%',
    },
    btnCloseMenu: {
        position: 'absolute',
        right: 0,
        top: 0,
    }
}));

const Default = props => {
    const classes = useStyles();

    const { children } = props;

    const [state, setState] = React.useState(false);

    const toggleDrawer = (open) => event => {
        setState(open);
    };

    return (
        <React.Fragment>
            <AppBar position="static">
                <Toolbar disableGutters>
                    <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu" onClick={toggleDrawer(true)}>
                        <MenuRoundedIcon />
                    </IconButton>
                    <Typography variant="h1" className={classes.pageHead}>페이지 제목 (H1)</Typography>
                </Toolbar>
                <Drawer open={state} onClose={toggleDrawer(false)} classes={{paper: classes.navigation}}>
                    sidebar
                    <IconButton aria-label="menu close" onClick={toggleDrawer(false)} className={classes.btnCloseMenu}>
                        <CloseRoundedIcon />
                    </IconButton>
                </Drawer>
            </AppBar>
            <div>
                {children}
            </div>
        </React.Fragment>
    );
};

Default.propTypes = {
    children: PropTypes.node
};

export default Default;