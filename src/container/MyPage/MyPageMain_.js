import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Link, Grid, Card, CardContent, IconButton, Typography, Button, Chip } from '@material-ui/core';
import { IcoMyPage } from '../../static/images';

const useStyles = makeStyles((theme) => ({
    aaa: {
        position: 'relative',
        top: -50,
        margin: '0 -20px -85px',
        padding: '50px 20px 75px',
        background: theme.palette.backgroundGradient,
    },
    welcomeMsg: {
        marginTop: 40,
        fontWeight: '300',
        fontSize: 30,
        lineHeight: 1.4,
        color: '#fff',
    },
    myGroup: {
        position: 'relative',
        marginTop: 25,
        paddingLeft: 11,
        color: '#fff',
        opacity: 0.6,
        '&:before': {
            display: 'block',
            position: 'absolute',
            left: 0,
            top: 9,
            width: 3,
            height: 3,
            borderRadius: '50%',
            backgroundColor: '#fff',
            opacity: 0.8,
            content: '""',
        },
        '& > span': {
            display: 'inline-flex',
            alignItems: 'center',
            '&:after': {
                marginLeft: 9,
                marginRight: 9,
                fontSize: 11,
                content: '">"',
            },
            '&:last-child:after': {
                display: 'none',
            }
        },
    },
    remindWrap: {
        marginTop: 25,
    },
    remind: {
        marginTop: 15,
    },
    card: {
        position: 'relative',
    }
}));

const MyPageMain = () => {
    const classes = useStyles();

    return (
        <React.Fragment>
            <div className="contents">
                <div className={classes.aaa}>
                    <Typography className={classes.welcomeMsg}>공지철님<br /> 반갑습니다 :D</Typography>
                    <div className={classes.myGroup}><span>중앙본부</span><span>중앙지사</span><span>중앙지점</span></div>
                    <Link href="#" className={classes.editMyInfo}>내정보 관리</Link>
                    <div className={classes.remindWrap}>
                        <dl className={classes.remind}>
                            <dt><strong>생일</strong> 맞으신 고객님</dt>
                            <dd><span>박보검</span></dd>
                            <dd><span>이동욱</span></dd>
                        </dl>
                        <dl className={classes.remind}>
                            <dt><strong>보험갱신</strong> 필요한 고객님</dt>
                            <dd><span>장동윤</span></dd>
                            <dd><span>박서준</span></dd>
                        </dl>
                    </div>
                </div>
                <Card className={classes.card}>
                    <Typography variant="h2" className={classes.title}>진행중인 신계약</Typography>
                    <CardContent>
                        <div className={classes.date}><span>기준일 2019-12-11</span></div>
                        <div className={classes.desc}><strong>3</strong> 건</div>
                        <div className={classes.desc}><strong>230,000</strong> 원</div>
                    </CardContent>
                </Card>
                <Card className={classes.card}>
                    <Typography variant="h2" className={classes.title}>보유 계약</Typography>
                    <CardContent>
                        <div className={classes.date}><span>기준일 2019-12-11</span></div>
                        <div className={classes.desc}><strong>236</strong> 건</div>
                        <div className={classes.desc}><strong>37,562,000</strong> 원</div>
                        <div className={classes.detail}>
                            <div>
                                <Chip size="small" variant="outlined" label="정상" className="chipLabel1" />
                                <strong>96</strong> 건 <span>/</span> <strong>27,562,000</strong> 원
                            </div>
                            <div>
                                <Chip size="small" variant="outlined" label="연체" className="chipLabel2" />
                                <strong>96</strong> 건 <span>/</span> <strong>27,562,000</strong> 원
                            </div>
                            <div>
                                <Chip size="small" variant="outlined" label="실효" className="chipLabel3" />
                                <strong>96</strong> 건 <span>/</span> <strong>27,562,000</strong> 원
                            </div>
                        </div>
                    </CardContent>
                </Card>
                <div>
                    banner
                </div>
            </div>
            <div className="stickyBottom">
                <Button variant="contained" fullWidth className="btnConfirm">버튼</Button>
            </div>
        </React.Fragment>
    );
};

export default MyPageMain;