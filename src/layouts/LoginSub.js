import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(theme => ({
    container: {
        paddingLeft: 20,
        paddingRight: 20,
    }
}));

const Default = props => {
    const classes = useStyles();

    const { children } = props;

    return (
        <React.Fragment>
            <div className={classes.container}>
                {children}
            </div>
        </React.Fragment>
    );
};

Default.propTypes = {
    children: PropTypes.node
};

export default Default;