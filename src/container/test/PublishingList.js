import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import { Table, TableHead, TableBody, TableRow, TableCell, List, ListItem, ListItemText } from '@material-ui/core';
import { data } from './data';

const useStyles = makeStyles((theme) => ({
    htmlDesc: {
        '& li': {
            position: 'relative',
            marginTop: 10,
            '&:first-child': {
                marginTop: 0,
            },
            '&:before': {
                display: 'block',
                position: 'absolute',
                left: 5,
                top: 9,
                width: 3,
                height: 3,
                borderRadius: '50%',
                background: theme.palette.primary.main,
                content: '""',
            }
        }
    },
    list: {
        margin: 0,
    },
    pubListWrap: {
        marginBottom: 20,
        padding: 30,
        '& li': {
            position: 'relative',
            marginTop: 10,
            '&:first-child': {
                marginTop: 0,
            },
            '&:before': {
                display: 'block',
                position: 'absolute',
                left: 5,
                top: 9,
                width: 3,
                height: 3,
                borderRadius: '50%',
                background: theme.palette.primary.main,
                content: '""',
            }
        }
    }
}));

const Test = () => {
    const classes = useStyles();
    return (
        <React.Fragment>
            <Table>
                <caption>퍼블리싱 목록</caption>
                <colgroup>
                    <col style={{width: '13%'}} />
                    <col style={{width: '15%'}} />
                    <col style={{width: '15%'}} />
                    <col style={{width: '10%'}} />
                    <col style={{width: '10%'}} />
                    <col style={{width: '10%'}} />
                    <col style={{width: '*'}} />
                </colgroup>
                <TableHead>
                    <TableRow>
                        <TableCell>Level 1</TableCell>
                        <TableCell>Level 2</TableCell>
                        <TableCell>Level 3</TableCell>
                        <TableCell>Level 4</TableCell>
                        <TableCell>ID</TableCell>
                        <TableCell>Path</TableCell>
                        <TableCell>Date</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                {data.map((row, index) => (
                    <TableRow key={index}>
                        <TableCell>{row.lv1}</TableCell>
                        <TableCell>{row.lv2}</TableCell>
                        <TableCell>{row.lv3}</TableCell>
                        <TableCell>{row.lv4}</TableCell>
                        <TableCell>{row.id}</TableCell>
                        <TableCell>
                            <RouterLink to={row.path} target="_blank">{row.path}</RouterLink>
                        </TableCell>
                        <TableCell>
                            <List disablePadding dense className={classes.htmlDesc}>
                                {row.summary.map((summary, i) => (
                                    <ListItem key={i}>
                                        <ListItemText primary={summary.date} secondary={summary.desc} className={classes.list} />
                                    </ListItem>
                                ))}
                            </List>
                        </TableCell>
                    </TableRow>
                ))}
                </TableBody>
            </Table>
        </React.Fragment>
    );
};

export default Test;