import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(theme => ({
    container: {
        position: 'relative',
        padding: '50px 20px 55px',
        '& .tabs': {
            marginLeft: -20,
            marginRight: -20,
        },
        '& .stickyBottom': {
            position: 'fixed',
            left: 0,
            bottom: 0,
            width: '100%',
            '& .btnConfirm': {
                width: '100%',
                height: 55,
                borderLeft: '1px solid rgba(248, 249, 252, 0.1)',
                borderRadius: 0,
                fontWeight: '700',
                '&:first-child': {
                    borderLeft: 'none',
                }
            },
        }
    }
}));

const Default = props => {
    const classes = useStyles();

    const { children } = props;

    return (
        <React.Fragment>
            <div className={classes.container}>
                {children}
            </div>
        </React.Fragment>
    );
};

Default.propTypes = {
    children: PropTypes.node
};

export default Default;