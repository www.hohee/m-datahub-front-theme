import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, TextField, InputAdornment, ButtonGroup, Button, IconButton, Typography, FormControlLabel, Checkbox } from '@material-ui/core';
import { ButtonClearInput } from '../../component';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import VisibilityOutlinedIcon from '@material-ui/icons/VisibilityOutlined';
import VisibilityOffOutlinedIcon from '@material-ui/icons/VisibilityOffOutlined';
import CheckBoxOutlinedIcon from '@material-ui/icons/CheckBoxOutlined';
import { Logo } from '../../static/images';

const useStyles = makeStyles((theme) => ({
    '@global': {
        '.formRow + .formRow': {
            marginTop: 10,
        }
    },
    logoArea: {
        margin: '100px auto 50px',
        textAlign: 'center',
    },
    logo: {
        maxWidth: 150
    },
    icoVisibility: {
        width: 22,
        height: 22,
        color: theme.palette.grey[500],
    },

    certifyArea: {
        flex: 1,
    },
    btnCertify: {
        marginLeft: 13,
        '&button': {
            paddingLeft: 17,
            paddingRight: 17,
        },
        '& button': {
            width: 59,
            paddingLeft: 0,
            paddingRight: 0,
        }
    },
    saveIdArea: {
        marginTop: 7,
    },
    checkboxLabel: {
        fontSize: 12,
    },
    loginMsg: {
        marginTop: 10,
    }
}));

const LoginMain = () => {
    const classes = useStyles();

    const [ visibilityTest, setVisibilityTest ] = React.useState(false);

    const handleClick = () => {
        visibilityTest ? setVisibilityTest(false) : setVisibilityTest(true);
    };

    return (
        <React.Fragment>
            <div className={classes.logoArea}>
                <img src={Logo} alt="(주)글로벌 금융 판매" className={classes.logo} />
            </div>
            <div className="mainFormGroup">
                <div className="formRow">
                    <TextField
                        id="userID"
                        /* error
                        helperText="아이디 오류" */
                        placeholder="아이디 입력"
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <ButtonClearInput />
                                </InputAdornment>
                            ),
                            classes: (
                                {
                                    input: "input inputOutline",
                                    notchedOutline: "notchedOutline"
                                }
                            ),
                            className: "inputBase"
                        }}
                        inputProps={
                            { "title" : "아이디 입력" }
                        }
                        fullWidth
                        variant="outlined"
                        className={clsx('textField iconTextField typeIcoUserId sizeLg')}
                    />
                </div>
                <div className="formRow">
                    <TextField
                        id="userPW"
                        type="password"
                        /* error
                        helperText="비밀번호 오류" */
                        placeholder="비밀번호 입력"
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <ButtonClearInput />
                                    <IconButton onClick={handleClick} className={classes.btnVisibility}>
                                        { visibilityTest ? <VisibilityOffOutlinedIcon classes={{ root: classes.icoVisibility }} /> : <VisibilityOutlinedIcon classes={{ root: classes.icoVisibility }} /> }
                                    </IconButton>
                                </InputAdornment>
                            ),
                            classes: (
                                {
                                    input: "input inputOutline",
                                    notchedOutline: "notchedOutline"
                                }
                            ),
                            className: "inputBase"
                        }}
                        inputProps={
                            { "title" : "비밀번호 입력" }
                        }
                        fullWidth
                        variant="outlined"
                        className={clsx('textField iconTextField typeIcoUserPw sizeLg')}
                    />
                </div>
                <Grid container direction="row" justify="space-between" alignItems="flex-start" className="formRow">
                    <Grid item className={classes.certifyArea}>
                        <TextField
                            id="certiNumber"
                            type="tel"
                            /* error
                            helperText="인증번호 오류" */
                            placeholder="인증번호 입력"
                            InputProps={{
                                endAdornment: (
                                    <InputAdornment position="end">
                                        <ButtonClearInput />
                                        <Typography color="error" className="timeLimit"><Typography variant="srOnly">인증번호 유효시간</Typography>5:00</Typography>
                                    </InputAdornment>
                                ),
                                classes: (
                                    {
                                        input: "input inputOutline",
                                        notchedOutline: "notchedOutline"
                                    }
                                ),
                                className: "inputBase"
                            }}
                            inputProps={
                                { "title" : "인증번호 입력" }
                            }
                            fullWidth
                            variant="outlined"
                            className={clsx('textField iconTextField typeIcoPhone sizeLg')}
                        />
                    </Grid>
                    <Grid item>
                        <ButtonGroup variant="outlined" size="large" className={clsx("btnGroup", classes.btnCertify)}>
                            <Button>재전송</Button>
                            <Button>인증</Button>
                        </ButtonGroup>
                        {/* <Button variant="outlined" size="large" className={clsx(classes.btnCertify)}>인증번호요청</Button> */}
                    </Grid>
                </Grid>
                <Typography className={clsx("msgGuide typeInfo sizeSmall", classes.loginMsg)}>안내메시지 영역</Typography>
                <Typography color="error" className={clsx("msgGuide typeError sizeSmall", classes.loginMsg)}>오류메시지 영역</Typography>
                <div className="pageActions">
                    <Button variant="contained" size="large" color="primary" fullWidth>로그인</Button>
                </div>
                <Grid container justify="space-between" alignItems="center" className={classes.saveIdArea}>
                    <Grid item><FormControlLabel control={<Checkbox icon={<span className="icon"></span>} checkedIcon={<span className="icon iconChecked"></span>} />}  label="아이디 저장" classes={{ label: classes.checkboxLabel }} /></Grid>
                    <Grid item>
                        <Button
                            endIcon={<ChevronRightIcon className="icoLinkTextArr" />}
                            classes={{ endIcon: 'endIcon' }}
                            className="btnLinkText"
                        >
                            아이디·비밀번호 찾기
                        </Button>
                    </Grid>
                </Grid>
            </div>
        </React.Fragment>
    );
};

export default LoginMain;