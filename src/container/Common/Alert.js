import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import { Dialog, DialogContent, DialogContentText, DialogActions, Button } from '@material-ui/core';
import { ButtonClearInput, IconComplete } from '../../component';

const useStyles = makeStyles((theme) => ({
    '@global': {
        '.formRow': {
            '& + .formRow, & + .boxRadioGroup': {
                marginTop: 10,
            }
        }
    },
}));

const Alert = () => {
    const classes = useStyles();
    
    return (
        <React.Fragment>
            <Dialog open aria-describedby="alertContent" classes={{ paper: 'alertPaper' }}>
                <DialogContent id="alertContent" className="alertContent">
                    <DialogContentText className="alertContentText">
                        비밀번호 5회 잘못 입력하였습니다.<br />
                        비밀번호를 초기화 해주십시오.
                    </DialogContentText>
                </DialogContent>
                <DialogActions disableSpacing className="alertActions">
                    <Button variant="contained" className="btnConfirm">secondary</Button>
                    <Button variant="contained" className="btnConfirm">primary</Button>
                </DialogActions>
            </Dialog>
        </React.Fragment>
    );
};

export default Alert;